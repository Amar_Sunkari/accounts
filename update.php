<?php

session_start();

date_default_timezone_set('Asia/Kolkata');

include 'connect.php';

$currentDate = date("Y-m-d");


if (isset($_POST['costcenter_update_password'])) {

    if($_POST['c_password'] != $_POST['c_password_confirm']){
        echo "<script> alert('Both Passwords do not match. Please enter again.'); window.location.href = 'index.php?forgotpassword=true&newpassword=true&token=" . $_POST['c_id']*1975 . "#toregister'; </script>";
    } else {
        $sql = "update costcenter set c_password = '" . $_POST['c_password'] . "' where c_id = ". $_POST['c_id'];

        if (mysqli_query($con, $sql)) {
            //echo "added";
        } else {
            echo "Error: " . $sql . "<br>" . mysqli_error($con);
        }
    
        echo "<script> alert('Password Updated Successfully. You can login with new password.'); window.location.href = 'index.php#toregister'; </script>";


    }

}


if (isset($_POST['costcenter_forgotpassword'])) {
    $sql = "select * from costcenter where c_username = '" . $_POST['c_username'] . "' ";
    $result = mysqli_query($con, $sql);

    if (mysqli_num_rows($result) == 0) {
        header('Location: index.php?forgotpassword=true&loginerror=true#toregister');
    } else {
        $row = mysqli_fetch_assoc($result);

        $token = $row['c_id']*1975;
        $recovery_otp = mt_rand(100000,999999);

        echo $token.'|'.$recovery_otp;

        sendSMS('91'.$row['c_hod_mobile'],'Hare Krishna, your OTP for password recovery is '.$recovery_otp.'. This is valid for next 15 min.');

        ?>
<!-- 
        <script> 
            var actual_otp = "<?php echo $recovery_otp; ?>";
            var flag = 0;
            var i = 0;

            while(flag==0) {

                var promptText = (i==0)?'Please Enter OTP sent to':'Incorrect OTP! Please enter correct OTP sent to.';

                var otp = prompt(promptText + ' <?php echo $row['c_hod_mobile']; ?>');
                if(otp == actual_otp){
                    flag = 1;
                }
                i++;    
            }
            
            
            window.location.href = 'index.php?forgotpassword=true&newpassword=true&token=<?php echo $token; ?>#toregister'; 
            </script>";
 -->
<?php

        // header('Location: index.php#toregister');
    }
}



if (isset($_POST['userlogin'])) {
    
    $sql = "select * from costcenter 
    inner join accesslevel 
    on c_a_id = accesslevel.a_id 
    where c_username = '" . $_POST['c_username'] . "' and c_password = '" . $_POST['c_password'] . "'";
    // echo $sql;

    $result = mysqli_query($con, $sql);

    if (mysqli_num_rows($result) == 0) {

        header('Location: index.php?loginerror=true');

    } else {

        $row = mysqli_fetch_assoc($result);
        $_SESSION['loggedin'] = $row;
        // $_SESSION['loggedin_id'] = $row['u_id'];
        // $_SESSION['loggedin_name'] = $row['u_name'];
        // if (($row['u_role']) == "admin") {
        //     $_SESSION['isadmin'] = true;
        // } else {
        //     $_SESSION['isadmin'] = false;
        // }

        header('Location: donations.php');
    }
}


if (isset($_GET['updateCostCenter'])) { 

    $sql = "UPDATE
                `costcenter`
            SET
                `c_name` = '".$_GET['c_name']."',
                `c_username` = '".$_GET['c_username']."',
                `c_opening_balance` = '".$_GET['c_opening_balance']."',
                `c_closing_balance` = '".$_GET['c_closing_balance']."',
                `c_advance` = '".$_GET['c_advance']."',
                `c_hod` = '".$_GET['c_hod']."',
                `c_hod_mobile` = '".$_GET['c_hod_mobile']."',
                `c_password` = '".$_GET['c_password']."',
                `c_a_id` = '".$_GET['c_a_id']."'
            WHERE
                c_id = ".$_GET['c_id'];

     // echo $sql;

    if (mysqli_query($con, $sql)) {
        // echo "added";
    } else {
        echo "Error: " . $sql . "<br>" . mysqli_error($con);
    }

    header('Location: costcenters.php?editcostcenter=true');

}

if (isset($_GET['saveCostCenter'])) { 


    $sql = "INSERT INTO `costcenter`(
    `c_last_updated_on`, 
    `c_name`,
    `c_username`,
    `c_opening_balance`,
    `c_closing_balance`,
    `c_advance`,
    `c_hod`,
    `c_sign`,
    `c_hod_mobile`,
    `c_password`,
    `c_a_id`
)
VALUES(
    '',
    '".$_GET['c_name']."',
    '".$_GET['c_username']."',
    '".$_GET['c_opening_balance']."',
    '".$_GET['c_closing_balance']."',
    '".$_GET['c_advance']."',
    '".$_GET['c_hod']."',
    '',
    '".$_GET['c_hod_mobile']."',
    '".$_GET['c_password']."',
    '".$_GET['c_a_id']."' )";

     // echo $sql;

    if (mysqli_query($con, $sql)) {
        // echo "added";
    } else {
        echo "Error: " . $sql . "<br>" . mysqli_error($con);
    }

    header('Location: costcenters.php?addcostcenter=true');

}



if (isset($_GET['save_receipt']) || isset($_GET['update_receipt'])) { 

    $t_payment_date = '';
    $t_payment_amount = '';
    $t_mode = $_GET['t_mode'];
    $t_credit = 0;
    $t_debit = 0;
    $t_detail = '';

    if($t_mode!='cheque') {
        $_GET['t_cheque_no']='';
    }

    if($t_mode == 'online') {
        $t_payment_amount = implode("|", $_GET['donationAmount']);
        foreach ($_GET['donationAmount'] as $value) {
          $t_credit+= $value;
        }
        $t_payment_date = implode("|", $_GET['paymentDate']);
        if(sizeof($_GET['donationAmount'])>1) {
            $_GET['t_is_part']=0;
            $_GET['t_actual_amount']='';
        }
    } else {
        $t_credit = $_GET['donation_amount'];
    }

    $t_donor_address = $_GET['t_donor_address_1'].'|'.$_GET['t_donor_address_2'].'|'.$_GET['t_donor_address_3'].'|'.$_GET['t_donor_address_4'];

    $sql = '';

    if(isset($_GET['save_receipt'])) {
       $sql = "INSERT INTO `transaction`(`t_date`, `t_approval_date`, `t_type_id`, `t_detail`, `t_mode`, `t_from_costcenter_id`, `t_to_costcenter_id`, `t_donor_name`, `t_donor_address`, `t_donor_pan`, `t_donor_mobile`, `t_payment_amount`, `t_payment_date`, `t_perm_receipt_no`, `t_temp_receipt_no`, `t_bank_id`, `t_cheque_no`, `t_payee`, `t_remarks`, `t_is_part`, `t_actual_amount`, `t_approve_status`, `t_debit`, `t_credit`) VALUES ('".$currentDate."','".$currentDate."','".$_GET['t_type_id']."','".$t_detail."','".$_GET['t_mode']."','".$_SESSION['loggedin']['c_id']."', '".$_GET['t_to_costcenter_id']."' , '".$_GET['t_donor_name']."','".$t_donor_address."','".$_GET['t_donor_pan']."','".$_GET['t_donor_mobile']."','".$t_payment_amount."', '".$t_payment_date."','',(SELECT COUNT(t_id) FROM (select * from transaction) AS t2 where t_type_id = 1 or t_type_id = 2) + 1,'".$_GET['t_bank_id']."','".$_GET['t_cheque_no']."','".$_GET['t_payee']."','".$_GET['t_remarks']."', '".$_GET['t_is_part']."' , '".$_GET['t_actual_amount']."'  , '', '".$t_debit."', '".$t_credit."' )"; 
    } else {

        $sql = "UPDATE
                    `transaction`
                SET
                    `t_date` = '".$currentDate."',
                    `t_to_costcenter_id` =  '".$_GET['t_to_costcenter_id']."' ,
                    `t_type_id` = '".$_GET['t_type_id']."',
                    `t_mode` = '".$_GET['t_mode']."' ,
                    `t_donor_name` = '".$_GET['t_donor_name']."',
                    `t_donor_address` = '".$t_donor_address."',
                    `t_donor_pan` = '".$_GET['t_donor_pan']."',
                    `t_donor_mobile` = '".$_GET['t_donor_mobile']."',
                    `t_payment_amount` = '".$t_payment_amount."',
                    `t_payment_date` = '".$t_payment_date."',
                    `t_bank_id` =  '".$_GET['t_bank_id']."',
                    `t_cheque_no` = '".$_GET['t_cheque_no']."',
                    `t_payee` = '".$_GET['t_payee']."',
                    `t_remarks` = '".$_GET['t_remarks']."',
                    `t_is_part` = '".$_GET['t_is_part']."',
                    `t_actual_amount` =  '".$_GET['t_actual_amount']."',
                    `t_credit` = '".$t_credit."' 
                WHERE
                    t_id = ".$_GET['t_id'];

    }
    

    // echo $sql;

    if (mysqli_query($con, $sql)) {
        //echo "added";
    } else {
        echo "Error: " . $sql . "<br>" . mysqli_error($con);
    }


        if(isset($_GET['save_receipt'])) {

            header('Location: donations.php?createreceipt=true');

        } else {

            header('Location: donations.php?editreceipt=true');

        }

        
}

if (isset($_GET['printDonations']) ) {

    header('Location: printreceipt.php?receiptCopyType='.$_GET['receiptCopyType'].'&receiptsPerPage='.$_GET['receiptsPerPage'].'&receipts='.implode('|', $_GET['selectedDonations']));
}

if (isset($_GET['approveDonations'])) {


     $i = 0;
     $donationSql;
     $effectedCostcenterArr;

    foreach ($_GET['selectedDonations'] as $value) {

        $donationData = explode('-', $_GET['selectedDonations'][$i]);
        
        $donationSql = "update transaction set 
        t_approval_date = '".$currentDate."' ,
        t_approve_status = '1',
        t_perm_receipt_no = (SELECT COUNT(t_id) FROM (select * from transaction) AS t2 WHERE t_approve_status = '1' and t_type_id = ".$donationData[1].") + 1 
        where t_id = '".$donationData[0]."' ";

        // echo $donationSql;

        if (mysqli_query($con, $donationSql)) {
        //echo "added";
        } else {
            echo "Error: " . $donationSql . "<br>" . mysqli_error($con);
        }


        $effectedCostcenterArr[$i]=$donationData[2];
        $i++;
        
    }

    $uniqueCostcenterArr = array_unique($effectedCostcenterArr);

    // echo implode(',', $uniqueCostcenterArr);

    updateClosingBalance($uniqueCostcenterArr);

    header('Location: donations.php?receipts_approved='.$i);

     

    }

function updateClosingBalance($costcentersArr){

    // echo implode(',', $costcentersArr);

    global $currentDate,$con;

    $conditionSql = '';

    $j = 1;

    foreach ($costcentersArr as $value) {
        $conditionSql = $conditionSql." when cc2.c_id = ".$value." then (select c".$j.".c_opening_balance from (select * from costcenter) as c".$j." where c".$j.".c_id = ".$value.") + (SELECT SUM(t_credit)-SUM(t_debit) FROM transaction WHERE t_approve_status =1 and t_to_costcenter_id = ".$value.") ";

        $j++;
    }

    $costcentersString = implode(',', $costcentersArr);

    $costcentersql = "UPDATE costcenter cc2
    SET 
    cc2.c_last_updated_on = '".$currentDate."', 
    cc2.c_closing_balance = (case                         
                            ".$conditionSql."
                        end)
    WHERE cc2.c_id in (".$costcentersString.") ";

    // echo $costcentersql;


    if (mysqli_query($con, $costcentersql)) {
        //echo "added";
        } else {
            echo "Error: " . $costcentersql . "<br>" . mysqli_error($con);
        }
}



if (isset($_POST['update_user'])) {

    $sql = "UPDATE `user` SET 
    `u_name`= '" . $_POST['u_name'] . "' , 
    `u_password`= '" . $_POST['u_password'] . "' ,
    `u_mobile`= '" . $_POST['u_mobile'] . "' ,
    `u_email`=  '" . $_POST['u_email'] . "' ,
    `u_role`= '" . $_POST['u_role'] . "' where u_id = " . $_POST['u_id'];

    echo $sql;

    if (mysqli_query($con, $sql)) {
        //echo "added";
    } else {
        echo "Error: " . $sql . "<br>" . mysqli_error($con);
    }

    header('Location: users.php');
}

if (isset($_POST['add_costcenter'])) {
   
    $sql = "INSERT INTO `costcenter`(`c_name`, `c_advance`, `c_credit`, `c_debit`, `c_user_id` ) VALUES (
        '" . $_POST['c_name'] . "' , 
        '" . $_POST['c_advance'] . "' , 
        '" . $_POST['c_credit'] . "' , 
        '" . $_POST['c_debit'] . "' , 
        '" . $_POST['c_user_id'] . "'  
        )";


    if (mysqli_query($con, $sql)) {
        //echo "added";
    } else {
        echo "Error: " . $sql . "<br>" . mysqli_error($con);
    }

    header('Location: costcenters.php');
}

if (isset($_POST['update_costcenter'])) {

    $sql = "UPDATE `costcenter` SET 
    `c_name`= '" . $_POST['c_name'] . "' , 
    `c_advance`= '" . $_POST['c_advance'] . "' ,
    `c_credit`= '" . $_POST['c_credit'] . "' ,
    `c_debit`=  '" . $_POST['c_debit'] . "' ,
    `c_user_id`= " . $_POST['c_user_id'] . " where c_id = " . $_POST['c_id'];

    // echo $sql;


    if (mysqli_query($con, $sql)) {
        //echo "added";
    } else {
        echo "Error: " . $sql . "<br>" . mysqli_error($con);
    }

    header('Location: costcenters.php');
}


if (isset($_POST['add_user'])) {
   
    $sql = "INSERT INTO `user`(`u_name`, `u_password`, `u_mobile`, `u_email`, `u_role` ) VALUES (
        '" . $_POST['u_name'] . "' , 
        '" . $_POST['u_password'] . "' , 
        '" . $_POST['u_mobile'] . "' , 
        '" . $_POST['u_email'] . "' , 
        '" . $_POST['u_role'] . "'  
        )";


    if (mysqli_query($con, $sql)) {
        //echo "added";
    } else {
        echo "Error: " . $sql . "<br>" . mysqli_error($con);
    }

    header('Location: users.php');
}






if (isset($_POST['create_transaction'])) {

    $t_type = $_POST['t_type']; 
    $date = date("Y-m-d");
    $type = $t_type[0]!='internal_transfer' ? ($t_type[0]=='payment'?$t_type[1]:$t_type[2]):$t_type[0];
    $credit = $t_type[0]=='payment'?$_POST['t_credit']:0;
    $debit = $t_type[0]=='payment'?0:$_POST['t_debit'];


    $sql = "INSERT INTO `transaction`(`t_date`, `t_type`, `t_detail`, `t_mode`, `t_costcenter_id`, `t_tocostcenter_id`, `t_user_id`, `t_debit`, `t_credit`) VALUES 
    ('".$date."','".$type."','".$_POST['t_detail']."','".$_POST['t_mode']."',".$_POST['t_costcenter_id'].",
    ".($type=='internal_transfer'?$_POST['t_tocostcenter_id']:0).",".$_POST['t_user_id'].",".$credit.",
    ".$debit.")";

    $_SESSION['create_transaction_sql'] = $sql;
    $_SESSION['approval_otp'] = rand(1000,9999);


        header('Location: createtransaction.php?approve=true');

    }

//-------------------------------------Only Reference Code Below---------------------------------------//

if (isset($_POST['teacher_update_password'])) {

    if($_POST['t_password'] != $_POST['t_password_confirm']){
        echo "<script> alert('Both Passwords do not match. Please enter again.'); window.location.href = 'index.php?forgotpassword=teacher&newpassword=true&token=" . $_POST['t_id']*73 . "#toregister'; </script>";
    } else {
        $sql = "update teacher set t_password = '" . $_POST['t_password'] . "' where t_id = ". $_POST[t_id];

        if (mysqli_query($con, $sql)) {
            //echo "added";
        } else {
            echo "Error: " . $sql . "<br>" . mysqli_error($con);
        }
    
        echo "<script> alert('Password Updated Successfully. You can login with new password.'); window.location.href = 'index.php#toregister'; </script>";


    }

}


if (isset($_POST['teacher_forgotpassword'])) {
    $sql = "select * from teacher where t_email = '" . $_POST['t_email'] . "' ";
    $result = mysqli_query($con, $sql);

    if (mysqli_num_rows($result) == 0) {
        header('Location: index.php?forgotpassword=teacher&loginerror=true#toregister');
    } else {
        $row = mysqli_fetch_assoc($result);

        $token = $row['t_id']*73;

        $to = $row['t_email'];
        $subject = "Password Recovery";
        $txt = "Hi " . $row['t_name'] . "\n Click on the following link to update your password \n ";
        $txt = $txt . "http://mylogin.arjunaedu.com/erp/index.php?forgotpassword=teacher&newpassword=true&token=".$token."#toregister";
        $txt = wordwrap($txt,70);
      
        $headers = "From: noreply@arjunaedu.com";

        mail($to,$subject,$txt,$headers);

        echo "<script> alert('A Recovery Email has been sent to ". $row['t_email'] . ". Follow the instructions to recover your password.'); window.location.href = 'index.php#toregister'; </script>";



        // header('Location: index.php#toregister');
    }
}


if (isset($_GET['select_center'])) {
    $_SESSION['center_id'] = $_GET['center_id'];
    $_SESSION['center_name'] = $_GET['center_name'];
    header('Location: teacher_dashboard.php');
}

if (isset($_GET['comment'])) {
    $sql = "insert into comment(`comment_subject`, `comment_content`, `teacher_id`, `student_id`, `viewed`) values('NA', '" . $_GET['comment'] . "', '" . $_GET['teacher_id'] . "', '" . $_GET['student_id'] . "', 0)";

    if (mysqli_query($con, $sql)) {
        echo "added";
    } else {
        echo "Error: " . $sql . "<br>" . mysqli_error($con);
    }
}


if (isset($_GET['logout'])) {

    session_destroy();

    header('Location: index.php');

   }

if (isset($_POST['save_timesheet'])) {
    $sql = "insert into `timesheet`(`ts_teacher_id`, `ts_date`, `ts_subject_id`, `ts_class_id`, `ts_course_id`, `ts_topic`, `ts_hours` ) values (" . $_SESSION['loggedin_id'] . ",'" . convertdatedb($_POST['ts_date']) . "'," . $_POST['ts_subject_id'] . "," . $_POST['ts_class_id'] . "," . $_POST['ts_course_id'] . ",'" . $_POST['ts_topic'] . "'," . $_POST['ts_hours'] . ")";

    //echo $sql;

    if (mysqli_query($con, $sql)) {
        //echo "added";
    } else {
        echo "Error: " . $sql . "<br>" . mysqli_error($con);
    }


    header('Location: timesheets.php');
}

if (isset($_GET['approvets'])) {
    if (sizeof($_GET['approvetimesheets']) > 0) {
        $sql = "update timesheet set ts_isApproved = 'yes' where ts_timesheet_id in (" . implode(",", $_GET['approvetimesheets']) . ")";

        //echo $sql;

        if (mysqli_query($con, $sql)) {
            //echo "added";
        } else {
            echo "Error: " . $sql . "<br>" . mysqli_error($con);
        }
    }


    header('Location: timesheets.php?teacher=' . $_GET['teacher']);
}

if (isset($_GET['delete_ts_timesheet_id'])) {
    $sql = "delete from timesheet where ts_timesheet_id = " . $_GET['delete_ts_timesheet_id'];

    //echo $sql;

    if (mysqli_query($con, $sql)) {
        //echo "added";
    } else {
        echo "Error: " . $sql . "<br>" . mysqli_error($con);
    }




    header('Location: timesheets.php?teacher=' . $_GET['teacher']);
}

if (isset($_GET['delete_course_id'])) {
    $sql = "delete from course where course_id = " . $_GET['delete_course_id'];

    //echo $sql;

    if (mysqli_query($con, $sql)) {
        header('Location: courses.php');
    } else {
        header('Location: courses.php?delete_error=true');
    }
}

if (isset($_GET['delete_subject_id'])) {
    $sql = "delete from subject where sub_id = " . $_GET['delete_subject_id'];

    //echo $sql;

    if (mysqli_query($con, $sql)) {
        header('Location: subjects.php');
    } else {
        header('Location: subjects.php?delete_error=true');
    }
}

if (isset($_GET['delete_class_id'])) {
    $sql = "delete from class where class_id = " . $_GET['delete_class_id'];

    //echo $sql;

    if (mysqli_query($con, $sql)) {
        header('Location: classes.php');
    } else {
        header('Location: classes.php?delete_error=true');
    }
}


if (isset($_POST['update_timesheet'])) {
    $sql = "UPDATE `timesheet` SET `ts_date`= '" . convertdatedb($_POST['ts_date']) . "',`ts_subject_id`=" . $_POST['ts_subject_id'] . ",`ts_class_id`= " . $_POST['ts_class_id'] . ",`ts_course_id`= " . $_POST['ts_course_id'] . " ,`ts_topic`= '" . $_POST['ts_topic'] . "' ,`ts_hours`= " . $_POST['ts_hours'] . "  WHERE ts_timesheet_id = " . $_POST['ts_timesheet_id'];

    //echo $sql;

    if (mysqli_query($con, $sql)) {
        //echo "added";
    } else {
        echo "Error: " . $sql . "<br>" . mysqli_error($con);
    }




    header('Location: timesheets.php?teacher=' . $_POST['teacher']);
}

if (isset($_POST['update_course'])) {
    $sql = "update course set course_title = '" . $_POST['course_title'] . "' , course_description = '" . $_POST['course_description'] . "'  where course_id = " . $_POST['course_id'];




    if (mysqli_query($con, $sql)) {
        //echo "added";
    } else {
        echo "Error: " . $sql . "<br>" . mysqli_error($con);
    }

    header('Location: courses.php');
}

if (isset($_POST['add_course'])) {


    /*$sql = "insert course set course_title = '".$_POST['course_title']."' , course_description = '".$_POST['course_description']."'  where course_id = ".$_POST['course_id'];*/

    $sql = "INSERT INTO `course`( `course_title`, `course_description`, `course_center_id`) VALUES ('" . $_POST['course_title'] . "' ,'" . $_POST['course_description'] . "'," . $_SESSION['center_id'] . ")";


    if (mysqli_query($con, $sql)) {
        //echo "added";
    } else {
        echo "Error: " . $sql . "<br>" . mysqli_error($con);
    }

    header('Location: courses.php');
}

if (isset($_POST['update_subject'])) {
    $sql = "update subject set sub_description = '" . $_POST['sub_description'] . "' where sub_id = " . $_POST['sub_id'];




    if (mysqli_query($con, $sql)) {
        //echo "added";
    } else {
        echo "Error: " . $sql . "<br>" . mysqli_error($con);
    }

    header('Location: subjects.php');
}

if (isset($_POST['add_subject'])) {


    /*$sql = "insert course set course_title = '".$_POST['course_title']."' , course_description = '".$_POST['course_description']."'  where course_id = ".$_POST['course_id'];*/

    $sql = "INSERT INTO `subject`( `sub_description`, `sub_center_id`) VALUES ('" . $_POST['sub_description'] . "' ," . $_SESSION['center_id'] . ")";


    if (mysqli_query($con, $sql)) {
        //echo "added";
    } else {
        echo "Error: " . $sql . "<br>" . mysqli_error($con);
    }

    header('Location: subjects.php');
}

if (isset($_POST['update_class'])) {
    $sql = "update class set class_description = '" . $_POST['class_description'] . "' where class_id = " . $_POST['class_id'];




    if (mysqli_query($con, $sql)) {
        //echo "added";
    } else {
        echo "Error: " . $sql . "<br>" . mysqli_error($con);
    }

    header('Location: classes.php');
}

if (isset($_POST['add_class'])) {


    /*$sql = "insert course set course_title = '".$_POST['course_title']."' , course_description = '".$_POST['course_description']."'  where course_id = ".$_POST['course_id'];*/

    $sql = "INSERT INTO `class`( `class_description`, `class_center_id`) VALUES ('" . $_POST['class_description'] . "' ," . $_SESSION['center_id'] . ")";


    if (mysqli_query($con, $sql)) {
        //echo "added";
    } else {
        echo "Error: " . $sql . "<br>" . mysqli_error($con);
    }

    header('Location: classes.php');
}

if (isset($_POST['apply_leave'])) {
    $sql = "SELECT * FROM leaves where " .
    "( leave_from_date <= '" . convertdatedb($_POST['leave_from_date']) . "' and leave_to_date >= '" . convertdatedb($_POST['leave_to_date']) . "' ) or " .
    "( leave_from_date >= '" . convertdatedb($_POST['leave_from_date']) . "' and leave_to_date <= '" . convertdatedb($_POST['leave_to_date']) . "' ) or " .
    "( leave_from_date >= '" . convertdatedb($_POST['leave_from_date']) . "' and leave_from_date <= '" . convertdatedb($_POST['leave_to_date']) . "' ) or " .
    "( leave_to_date >= '" . convertdatedb($_POST['leave_from_date']) . "' and leave_to_date <= '" . convertdatedb($_POST['leave_to_date']) . "' ) ";



    $result = mysqli_query($con, $sql);


    if (mysqli_num_rows($result) == 0) {
        $sql = "INSERT INTO `leaves`(`leave_teacher_id`, `leave_from_date`, `leave_to_date`, `leave_application_date`, `leave_purpose`) VALUES (" . $_SESSION['loggedin_id'] . ",'" . convertdatedb($_POST['leave_from_date']) . "','" . convertdatedb($_POST['leave_to_date']) . "','" . date("Y-m-d") . "','" . $_POST['leave_purpose'] . "')";


        if (mysqli_query($con, $sql)) {
        } else {
            echo "Error: " . $sql . "<br>" . mysqli_error($con);
        }


        header('Location: leaves.php');
    } else {
        header('Location: applyleave.php?apply_leave_error=true');
    }
}



if (isset($_GET['approve_leaves'])) {
    if (isset($_GET['approveleaves']) && sizeof($_GET['approveleaves']) > 0) {
        $leavestoapprove = $_GET['approveleaves'];
        $updatecasualleave = " ";
        $updatelossofpayleave = " ";
        $udpateleavedays = " ";

        $leaveids[0] = 0;
        $teacherids[0] = 0;




        $i = 0;


        foreach ($leavestoapprove as $iterator) {
            
        //for($i=0;$i<=sizeof($leavestoapprove);$i++){

            $approveleavedata = explode("|", $iterator);
            $leaveids[$i] = $approveleavedata[0];
            $teacherids[$i] = $approveleavedata[1];
            $casualleaves = $approveleavedata[2];
            $lopleaves = $approveleavedata[3];
            $leavedays = $approveleavedata[4];
            $udpateleavedays = $udpateleavedays . " WHEN " . $leaveids[$i] . " THEN " . $leavedays . " ";



            if ($casualleaves >= $leavedays) {
                $updatecasualleave = $updatecasualleave . " WHEN " . $teacherids[$i] . " THEN " . ($casualleaves - $leavedays) . " ";
                $updatelossofpayleave = $updatelossofpayleave . " WHEN " . $teacherids[$i] . " THEN " . $lopleaves . " ";
            } else {
                $updatecasualleave = $updatecasualleave . " WHEN " . $teacherids[$i] . " THEN 0 ";
                $updatelossofpayleave = $updatelossofpayleave . " WHEN " . $teacherids[$i] . " THEN " . ($leavedays - $casualleaves + $lopleaves) . " ";
            }

            $i++;
        }

        $teachersql = "update teacher set t_casual_leave = (CASE t_id " . $updatecasualleave . " END), t_lossofpay = (CASE t_id " . $updatelossofpayleave . " END) WHERE t_id in (" . implode(",", $teacherids) . ")";

        //echo $teachersql."\n";

        if (mysqli_query($con, $teachersql)) {
            //echo "added";
        } else {
            echo "Error: " . $teachersql . "<br>" . mysqli_error($con);
        }




        $leavesql = "update leaves set leave_isApproved = 'yes', leave_days = (CASE leave_id " . $udpateleavedays . " END)  where leave_id in (" . implode(",", $leaveids) . ")";

        //echo $leavesql."\n";

        if (mysqli_query($con, $leavesql)) {
            //echo "added";
        } else {
            echo "Error: " . $sql . "<br>" . mysqli_error($con);
        }
    }


    header('Location: leaves.php?teacher=' . $_GET['teacher']);
}

if (isset($_GET['delete_leave_id'])) {
    $sql = "delete from leaves where leave_id = " . $_GET['delete_leave_id'];

    //echo $sql;

    if (mysqli_query($con, $sql)) {
        //echo "added";
    } else {
        echo "Error: " . $sql . "<br>" . mysqli_error($con);
    }


    header('Location: leaves.php?teacher=' . $_GET['teacher']);
}

if (isset($_POST['update_leave'])) {
    $sql = "UPDATE `leaves` SET `leave_from_date`='" . convertdatedb($_POST['leave_from_date']) . "',`leave_to_date`='" . convertdatedb($_POST['leave_to_date']) . "',`leave_application_date`='" . date("Y-m-d") . "',`leave_purpose`='" . $_POST['leave_purpose'] . "' WHERE leave_id = " . $_POST['leave_id'];
    ;

    //echo $sql;

    if (mysqli_query($con, $sql)) {
        //echo "added";
    } else {
        echo "Error: " . $sql . "<br>" . mysqli_error($con);
    }

    header('Location: leaves.php?teacher=' . $_POST['teacher']);
}


if (isset($_GET['delete_holiday_id'])) {
    $sql = "delete from holiday where holiday_id = " . $_GET['delete_holiday_id'];

    //echo $sql;

    if (mysqli_query($con, $sql)) {
        //echo "deleted";
    } else {
        echo "Error: " . $sql . "<br>" . mysqli_error($con);
    }

    header('Location: holidays.php');
}

if (isset($_POST['update_holiday'])) {
    $sql = "update holiday set holiday_date = '" . convertdatedb($_POST['holiday_date']) . "' , holiday_description = '" . $_POST['holiday_description'] . "'  where holiday_id = " . $_POST['holiday_id'];

    if (mysqli_query($con, $sql)) {
        //echo "added";
    } else {
        echo "Error: " . $sql . "<br>" . mysqli_error($con);
    }

    header('Location: holidays.php');
}

if (isset($_POST['add_holiday'])) {


    /*$sql = "insert course set course_title = '".$_POST['course_title']."' , course_description = '".$_POST['course_description']."'  where course_id = ".$_POST['course_id'];*/

    $sql = "INSERT INTO `holiday`( `holiday_date`, `holiday_description`, `holiday_center_id`) VALUES ('" . convertdatedb($_POST['holiday_date']) . "' ,'" . $_POST['holiday_description'] . "'," . $_SESSION['center_id'] . ")";


    if (mysqli_query($con, $sql)) {
        //echo "added";
    } else {
        echo "Error: " . $sql . "<br>" . mysqli_error($con);
    }

    header('Location: holidays.php');
}

if (isset($_POST['update_student'])) {

    $sql = "UPDATE `student` SET 
    `s_name`= '" . $_POST['s_name'] . "' , 
    `s_roll_no`= '" . $_POST['s_roll_no'] . "' ,
    `s_password`= '" . $_POST['s_password'] . "' ,
    `s_mobile`= '" . $_POST['s_mobile'] . "' ,
    `s_email`=  '" . $_POST['s_email'] . "' ,
    `s_dob`= '" . $_POST['s_dob'] . "' ,
    `s_address`= '" . $_POST['s_address'] . "' ,
    `s_father_name`=  '" . $_POST['s_father_name'] . "' ,
    `s_mother_name`= '" . $_POST['s_mother_name'] . "' ,
    `s_parent_mobile`= '" . $_POST['s_parent_mobile'] . "' ,
    `s_parent_email`= '" . $_POST['s_parent_email'] . "' where s_id = " . $_POST['s_id'];


    if (mysqli_query($con, $sql)) {
        //echo "added";
    } else {
        echo "Error: " . $sql . "<br>" . mysqli_error($con);
    }

    header('Location: student_list.php?course='.$_POST['course'].'&class='.$_POST['class'].'&getStudents=');
}


if (isset($_POST['upload_students'])) {

    if($_FILES['file_name']['tmp_name']){

        $handle = fopen($_FILES['file_name']['tmp_name'], "r");
        $count = 0;
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {

                if($count != 0){
            
                $importSql= "INSERT INTO `student`(`s_name`, `s_password`, `s_mobile`, `s_email`, `s_dob`, `s_course_id`, `s_roll_no`, `s_address`, `s_father_name`, `s_mother_name`, `s_parent_mobile`, `s_parent_email`, `s_center_id`, `s_class_id`) VALUES ( " .
                    
                "'" . $data[0] . "' , " .
                "'" . $data[1] . "' , " .
                "'" . $data[2] . "' , " .
                "'" . $data[3] . "' , " .
                "'" . convertdatedb($data[4]) . "' , " .
                "'" . $_POST['s_course_id'] . "' , " .
                "'" . $data[5] . "' , " .
                "'" . $data[6] . "' , " .
                "'" . $data[7] . "' , " .
                "'" . $data[8] . "' , " .
                "'" . $data[9] . "' , " .
                "'" . $data[10] . "' , " .
                "'" . $_POST['s_center_id'] . "' , " .
                "'" . $_POST['s_class_id'] . "'     
                    
                )";

                if (mysqli_query($con, $importSql)) {
                    //echo "added";
                } else {
                    echo "Error: " . $sql . "<br>" . mysqli_error($con);
                }

                }

                $count++;
            }
        fclose($handle);
        // print "Import done";
    }

    header('Location: student_list.php?upload_successful=true');

}

if (isset($_POST['upload_attendance'])) {

    if($_FILES['file_name']['tmp_name']){

        $dateRegex = "/^(0[1-9]|[12][0-9]|3[01])[-](0[1-9]|1[012])[-]\\d\\d\\d\\d$/";
        $handle = fopen($_FILES['file_name']['tmp_name'], "r");
        $count = 0;
        $dateCols = 0;
        $dbdate;
        $dates = array();
        $flag = true;
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                

                if($count != 0){

                    for($i = 0; $i < $dateCols; $i++){

                        if($data[$i + 3] != 0 && $data[$i + 3] != 1)
                            header('Location: upload_attendance.php?format_error=true'); 

                        $dbdate = convertdatedb($dates[$i]);

                        $importSql = "INSERT INTO `attendance`(`a_date`, `a_status`, `a_student_id`, `a_unique_key`) VALUES ( " .
                        
                        "'" . $dbdate . "' , " .
                        "'" . $data[$i + 3] . "' , " .
                        "'" . $data[0] . "' , " .
                        "'" . $dbdate . "|" . $data[0] . "'              

                        )";
                    
                        if (mysqli_query($con, $importSql)) {
                            //echo "added";
                        } else {
                            // echo "Error: " . $importSql . "<br>" . mysqli_error($con);

                            $updateSql = "update attendance set a_status = '" . $data[$i + 3] . "' where a_unique_key = '" .$dbdate . "|" . $data[0]. "'";
                            mysqli_query($con, $updateSql);
                            
                        }
                    }


                } else {
                    $dateCols = count($data) - 3;
                    for($i = 0; $i < $dateCols; $i++){
                        if(preg_match($dateRegex, $data[$i+3])==1)
                            $dates[$i] = $data[$i+3];
                        else{
                            header('Location: upload_attendance.php?date_error=true'); 
                            $flag = false;
                        }
                    }

                }

                $count++;
            }
        fclose($handle);
        // print "Import done";
    }
    if($flag)
    header('Location: student_list.php?upload_successful=true');

}

    if(isset($_GET['testcode'])){
        // $dateRegex = "/^(0[1-9]|[12][0-9]|3[01])[-](0[1-9]|1[012])[-]\\d\\d\\d\\d$/";
        // echo preg_match($dateRegex, $_GET['date']);

        sendSMS('919775030354','Hare Krishna P, PAMHO Please accept my humble obesances');

    }

