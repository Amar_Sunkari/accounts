<?php

session_start();

require('fpdf.php');

include('connect.php');

date_default_timezone_set('Asia/Kolkata');

class PDF extends FPDF
{
/*  function Header()
    {
      $this->Image('logo.png',10,8,33);
      $this->SetFont('Helvetica','B',15);
      $this->SetXY(50, 10);
      $this->Cell(0,10,'This is a header',1,0,'C');
     }*/




  function Footer()
    {

      $this->SetXY(40,-8);
      $this->SetFont('Helvetica','I',10);
      $this->Write (5, 'Printed on '.date("d-m-Y H:i:s"));
    }
}

$pdf=new PDF();
$pdf->SetAutoPageBreak(true, 2);

if(isset($_GET['receipts'])) {
  
  $receiptsArr = explode('|', $_GET['receipts']);

  $i = 0;
  $receiptIdArr;

  foreach ($receiptsArr as $value) {
    
    $receiptIdArr[$i] = explode('-', $value)[0];

    $i++;

  }

  $receiptIdString = implode(',', $receiptIdArr);

  $receiptsql = "select * from transaction 
  inner join costcenter 
  on t_to_costcenter_id = costcenter.c_id 
  inner join bank 
  on t_bank_id = bank.b_id 
  where t_id in (".$receiptIdString.") ";

  $result = mysqli_query($con, $receiptsql);


  $i = 1;

  $offset = 0;

  while($receiptRow = mysqli_fetch_assoc($result)){

    $receiptsPerPage = $_GET['receiptsPerPage'];

    if($receiptsPerPage==1) {

      $pdf->AddPage();
      $offset = 0;

    } else {

      if($i % 2 != 0) {

        $pdf->AddPage();
        $offset = 0;

      } else {

        $pdf->Line(10, 140, 200, 140);
        $offset = 145;

      }

    }

    printReceipts($receiptRow, $offset);

    $i++;

  }

$pdf->Output();

}


function printReceipts($receiptRow, $offset) {

  global $pdf;

$pdf->Image('receipttemplates/receipt_bg.jpg',0,$offset + 0,210);

// $pdf->SetDrawColor(225,54,135);
$pdf->SetLineWidth(0.4);
// $pdf->Rect(3.5, 3.5, 204, 130);
// $pdf->Rect(42, 18, 90, 22.5 );
// $pdf->Rect(8, 44.5, 104, 13.5 );
// $pdf->Rect(8, 62, 104, 30 );
// $pdf->Rect(8, 96, 104, 18 );
// $pdf->Rect(115, 44.5, 87, 47 );
// $pdf->Rect(8,120, 194, 8 );
$pdf->SetTextColor(61,64,148);
$pdf->SetRightMargin(5);
$pdf->SetXY(20,$offset + 5);
$pdf->SetFont('Times','B',19);
$pdf->Write(5,'International Society for Krishna Consciousness (ISKCON)');

$pdf->SetXY(45,$offset + 12);
$pdf->SetFont('Times','',11);
$pdf->Write(4,'Founder-Acharya: His Divine Grace A.C. Bhaktivedanta Swami Prabhupada');

$pdf->SetTextColor(0,0,0);
$pdf->SetXY(62,$offset + 21);
$pdf->SetFont('Times','B',12);
$pdf->Write(4,'Branch: ISKCON New Town');

$pdf->SetLeftMargin(47);
$pdf->SetRightMargin(78);
$pdf->SetXY(47,$offset + 26);
$pdf->SetFont('Times','',10.5);
$pdf->Write(4,'Near Shapoorji Sukhobrishti Complex, Opp. to Rabindra Bharati School, NewTown, Kolkata-700135                 Mobile No: 9830663964, 7890056852                         Email: treasury.iskconnewtown@gmail.com');

$pdf->SetTextColor(237,51,56);
$pdf->SetRightMargin(5);
$pdf->SetXY(90,$offset + 46.5);
$pdf->SetFont('Times','',11);
$pdf->Write(3,'Donation Amount');

$pdf->SetXY(50,$offset + 61);
$pdf->Write(3,'Donor Details');

$pdf->SetXY(140,$offset + 105);
$pdf->Write(3,'Authorised Signatory');

if($receiptRow['t_approve_status']==1 && $receiptRow['t_type_id']==2) {

$pdf->SetXY(32,$offset + 105);
$pdf->Write(3,'Income Tax Exemption (80-G) Number');

$pdf->SetTextColor(0,0,0);
$pdf->SetXY(40,$offset + 109);
$pdf->SetFont('Times','',11);
$pdf->Write(3,'80G/1667/2007/2008-2009');

$pdf->SetFont('Times','',9);
$pdf->SetXY(12.5,$offset + 112.5);
$pdf->Write(3,'Validity extended perpetually vide CBDT circular No. 7/2020 - 27/10/2010');

}

// $pdf->SetXY(15,114);
// $pdf->Write(3,'CBDT circular No. 7/2020. Dated: 27/10/2010');

$receiptNoLabel = ($receiptRow['t_approve_status']==1)?'Permanent Receipt No.:':(($receiptRow['t_approve_status']==0)?'Temporary Receipt No.:':'RECEIPT CANCELLED');

$pdf->SetTextColor(225,54,135);
$pdf->SetXY(130.5,$offset + 18);
$pdf->SetFont('Times','B',13);
$pdf->Write(3,$receiptNoLabel);


$pdf->SetXY(145,$offset + 61);
$pdf->SetFont('Times','',11);
$pdf->Write(3,'Payment Details');



$pdf->SetTextColor(0,0,0);
$pdf->SetXY(22,$offset + 122);
$pdf->SetFont('Times','',10);
$pdf->Write(3,'Registered Office: Hare Krishna Land, Juhu, Mumbai - 400049. Tel: (022) 2628 6579 E-mail: info@iskconindia.org');

$pdf->SetXY(24,$offset + 126);
$pdf->SetFont('Times','',10);
$pdf->Write(3,'Registered under Maharashthra Public Trust Act 1950, vide Registration No. F-2179 (Bom). PAN: AAATI0017P');

// All the dynamic fields start below:

$addressArr = explode('|', $receiptRow['t_donor_address']);
$arrSize = count($addressArr);
$addressArr[$arrSize-2] = $addressArr[$arrSize-2].' - '.$addressArr[$arrSize-1];
unset($addressArr[$arrSize-1]);
$donor_address=implode(', ', $addressArr);

$receipt_no = '';
$receiptType = ($receiptRow['t_type_id']==1)?'N80G ':'80G ';

switch ($receiptRow['t_approve_status']) {
  case 0:
    $receipt_no = 'TEMPRCT '.$receiptType.sprintf('%05d', $receiptRow['t_temp_receipt_no']);
    break;
  case 1:
    $receipt_no = '1369 02 '.$receiptType.sprintf('%05d', $receiptRow['t_perm_receipt_no']);
    break;
  case 2:
    $receipt_no = '';
    break;
}

$receipt_copy = "Donor's Copy";
$receipt_date = convertdatenormal($receiptRow['t_date']);
$donation_amount=$receiptRow['t_credit'];
$donor_name = $receiptRow['t_donor_name'];
// $donor_address = str_replace('|', ', ', $receiptRow['t_donor_address']);
$donor_mobile = $receiptRow['t_donor_mobile'];
$donor_pan = strtoupper($receiptRow['t_donor_pan']);
$costcenter = $receiptRow['c_name'];
$payment_mode = $receiptRow['t_mode'];
$cheque_no = $receiptRow['t_cheque_no'];
$bank_name = $receiptRow['b_name'].'-'.$receiptRow['b_account_no'];
$dated = '20-12-2020';

/*$pdf->SetTextColor(0,0,0);
$pdf->SetXY(7,38);
$pdf->SetFont('Times','B',14);
$pdf->Write(3, $receipt_copy);*/

$pdf->SetXY(130.5,$offset + 25);
$pdf->SetFont('Courier','',17.5);
$pdf->Write(3,$receipt_no);

/*$pdf->SetXY(135,32);
$pdf->SetFont('Times','B',11);
$pdf->Write(3,'Receipt Type: '.$receipt_type);*/

$pdf->SetTextColor(61,64,148);
$pdf->SetXY(148,$offset + 36);
$pdf->SetFont('Times','B',13);
$pdf->Write(3,'Date: '.$receipt_date);


$pdf->SetLeftMargin(16);
$pdf->SetRightMargin(70);
$pdf->SetXY(16,$offset + 49.5);
$pdf->SetFont('Times','B',12);
$pdf->Write(4,'Rupees '.ucwords(getIndianCurrency($donation_amount)));

$pdf->SetRightMargin(8);
$pdf->SetXY(150,$offset + 50.5);
$pdf->SetFont('Times','B',16);
$pdf->Write(3,moneyFormatIndia($donation_amount));

$pdf->SetFont('Times','B',11.5);
$pdf->SetXY(16,$offset + 67);
$pdf->Write(3,'Name: '.ucwords($donor_name));

$pdf->SetRightMargin(100);
$pdf->SetXY(16,$offset + 74);
$pdf->Write(5,'Address: '.ucwords($donor_address));

$pdf->SetXY(16,$offset + 93);
$pdf->Write(4,'Mobile: '.$donor_mobile);

$pdf->SetXY(58,$offset + 93);
$pdf->Write(4,'PAN: '.$donor_pan);

$pdf->SetFont('Times','B',11);
$pdf->SetRightMargin(8);
$pdf->SetLeftMargin(116);
$pdf->SetXY(116,$offset + 65);
$pdf->Write(4,'Payment Mode: '.ucwords($payment_mode).';');

if($payment_mode=='cheque') {
  $pdf->SetXY(162,$offset + 65);
  $pdf->Write(4,'Cheque No: '.$cheque_no);
}

  
$pdf->SetXY(116,$offset + 70);
$pdf->Write(4,'Bank: '.$bank_name);

if($payment_mode=='online') {

  if($receiptRow['t_is_part']==1) {

    $pdf->SetXY(116,$offset + 75);
    $pdf->Write(5,'Paid On: '.$receiptRow['t_payment_date']);

    if($_GET['receiptCopyType']==2) {
      
      $pdf->SetXY(116,$offset + 80);
      $pdf->Write(5,'Receipt made as a partial claim for the actual donation of Rs. '.moneyFormatIndia($receiptRow['t_actual_amount']));      

    }

  } else {

    $dateArr = explode('|', $receiptRow['t_payment_date']);
    $amountArr = explode('|', $receiptRow['t_payment_amount']);
    $j=0;

    foreach ($dateArr as  $value) {
    
    $pdf->SetXY(116,$offset + 75 + 4.5*$j);  
    $pdf->Write(4,'Paid On: '.$value.'; Amount: Rs. '.moneyFormatIndia($amountArr[$j]));
    $j++;

    }

  }

 

}

if($_GET['receiptCopyType']==2) {

    $pdf->SetTextColor(0,0,0);
    $pdf->SetFont('Times','',10);
    $pdf->SetXY(114,$offset + 132);
    $pdf->Write(5,'Cost Center: '.$receiptRow['c_name']);
  }

// $pdf->SetXY(118,$offset + 55);
// $pdf->Write(4,'Bank Name: '.$bank_name);

// $pdf->SetXY(118,$offset + 60);
// $pdf->Write(4,'Dated: '.$dated);



$pdf->Image('signs/'.$receiptRow['c_sign'],145,$offset + 109,24);

/*$pdf->SetRightMargin(5);
$pdf->SetXY(140,130);
$pdf->SetFont('Times','',8);
$pdf->Write(4,'Cost Center: '.$costcenter);
*/

}

?>