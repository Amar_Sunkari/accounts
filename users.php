<?php 

session_start();

if (!isset($_SESSION['isadmin']) || !$_SESSION['isadmin'])

  header('Location: index.php');

include 'header.php';

$dowMap = array('admin', 'accountant', 'hod', 'regular_user');

?>

<!-- page content -->

<style type="text/css">
.modal-dialog{
  overflow-y: initial !important
}
.modal-body{
  height: 300px;
  overflow-y: auto;
}

</style>
<link href="css/jquery-ui.css" rel="stylesheet">

<div class="right_col" role="main">
  <div class="">
    <div class="page-title">

    </div>
    <div class="clearfix"></div>



  </div>
  <div class="clearfix"></div>



  <div class="row">

    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h2>Users<small></small></h2>

          <div class="clearfix"></div>
        </div>
        <div class="x_content">

          <table id="datatable-responsive" class="table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
              <tr>
                <th>Update</th>
                <th>Sl No</th>
                <th>Name</th>
                <th>Mobile No</th>
                <th>Email Id</th>
                <th>Password</th>
                <th>Role</th>
              </tr>
            </thead>
            <tbody>

              <?php



              $usersql = "select * from user";
              

              $result = mysqli_query($con, $usersql);

              $slno=0;

              while ($userrow = mysqli_fetch_assoc($result)) {
                $slno++;


                ?>

                <tr>

                  <td><button type="button" class="btn btn-success btn-xs" data-toggle="modal" data-target=".bs-example-modal-sm"
                    onclick="
                    $('#u_name').val('<?php echo $userrow['u_name'] ?>');
                    $('#u_email').val('<?php echo $userrow['u_email'] ?>');
                    $('#u_password').val('<?php echo $userrow['u_password'] ?>');
                    $('#u_mobile').val('<?php echo $userrow['u_mobile'] ?>');
                    $('#u_role').val('<?php echo $userrow['u_role'] ?>');
                    $('#u_id').val('<?php echo $userrow['u_id'] ?>');
                    $('#user_update').attr('name','update_user');
                    
                    "><i
                    class="fa fa-edit"></i>&nbsp&nbspEdit</button>
                  </td>

                   <td>
                    <?php echo $slno ?>
                  </td>

                  <td>
                    <?php echo $userrow['u_name'] ?>
                  </td>
                   <td>
                    <?php echo $userrow['u_mobile'] ?>
                  </td>
                  <td>
                    <?php echo $userrow['u_email'] ?>
                  </td>
                  <td>
                    <?php echo $userrow['u_password'] ?>
                  </td>
                  <td>
                    <?php echo $userrow['u_role'] ?>
                  </td>

                  </tr>

                  <?php

                }
                ?>
              </tbody>
            </table>


          </div>
        </div>
      </div>
      <div class="col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 50px;">

        <button type="button" class="btn btn-warning" data-toggle="modal"  data-target=".bs-example-modal-sm"
        
        onclick="
                    $('#u_name').val('');
                    $('#u_email').val('');
                    $('#u_password').val('');
                    $('#u_mobile').val('');
                    $('#u_role').val(''); 
                    $('#u_id').val('');
                    $('#user_update').attr('name','add_user');
                    ">
        Add New
        User </button>

      </div>
    </div>







    <div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-hidden="true" id="comment_modal">
      <div class="modal-dialog modal-sm">
        <div class="modal-content">

          <form action="update.php" method="post">

            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
              </button>
              <h4 class="modal-title" id="myModalLabel2">User Details</h4>
            </div>
            <div class="modal-body">


              <input type="hidden" name="u_id" id="u_id">


              <div class="form-group">
                <label class="control-label col-md-4 col-sm-4 col-xs-6">Name<span class="required"></span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input id="u_name" class="form-control col-md-12 col-xs-12" type="text" name="u_name" required>
                </div>
              </div>
              <br>
              <br>

               <div class="form-group">
                <label class="control-label col-md-4 col-sm-4 col-xs-6">Email id<span class="required"></span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input id="u_email" class="form-control col-md-12 col-xs-12" type="text" name="u_email" required>
                </div>
              </div>
              <br>
              <br>


              <div class="form-group">
                <label class="control-label col-md-4 col-sm-4 col-xs-6">Password<span class="required"></span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input id="u_password" class="form-control col-md-12 col-xs-12" type="text" name="u_password" required>
                </div>
              </div>
              <br>
              <br>

              <div class="form-group">
                <label class="control-label col-md-4 col-sm-4 col-xs-6">Mobile No<span class="required"></span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input id="u_mobile" class="form-control col-md-12 col-xs-12" type="text" name="u_mobile" required>
                </div>
              </div>
              <br>
              <br>
                


              <div class="form-group">
                <label class="control-label col-md-4 col-sm-4 col-xs-6">Role<span class="required"></span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <select class="form-control" name="u_role" id="u_role" required>

                    <option value="">Select</option>
                    <option value="admin">Admin</option>
                    <option value="accountant">Accountant</option>
                    <option value="hod">HOD</option>
                    <option value="regular_user">Regular User</option>

                  </select>
                </div>
              </div>

              <br>
              <br>

              <br>
              <br>


            </div>
            <div class="modal-footer">


              <button class="btn btn-primary" type="submit" id = "user_update" name="update_user">Update</button>
            </div>

          </form>

        </div>
      </div>
    </div>





    <!-- footer content -->

    <?php include 'footer.php'; ?>
    <!-- /footer content -->

  </div>
  <!-- /page content -->
</div>

</div>

<div id="custom_notifications" class="custom-notifications dsp_none">
  <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
  </ul>
  <div class="clearfix"></div>
  <div id="notif-group" class="tabbed_notifications"></div>
</div>

<script src="js/bootstrap.min.js"></script>

<!-- bootstrap progress js -->
<script src="js/progressbar/bootstrap-progressbar.min.js"></script>
<script src="js/nicescroll/jquery.nicescroll.min.js"></script>
<!-- icheck -->
<script src="js/icheck/icheck.min.js"></script>

<script src="js/custom.js"></script>



<!-- Datatables -->
<!-- <script src="js/datatables/js/jquery.dataTables.js"></script>
  <script src="js/datatables/tools/js/dataTables.tableTools.js"></script> -->

  <!-- Datatables-->
  <!-- <script src="js/jquery.min.js"></script> -->
  <script src="js/datatables/jquery.dataTables.min.js"></script>
  <script src="js/datatables/dataTables.bootstrap.js"></script>
  <script src="js/datatables/dataTables.buttons.min.js"></script>
  <script src="js/datatables/buttons.bootstrap.min.js"></script>
  <script src="js/datatables/jszip.min.js"></script>
  <script src="js/datatables/pdfmake.min.js"></script>
  <script src="js/datatables/vfs_fonts.js"></script>
  <script src="js/datatables/buttons.html5.min.js"></script>
  <script src="js/datatables/buttons.print.min.js"></script>
  <script src="js/datatables/dataTables.fixedHeader.min.js"></script>
  <script src="js/datatables/dataTables.keyTable.min.js"></script>
  <script src="js/datatables/dataTables.responsive.min.js"></script>
  <script src="js/datatables/responsive.bootstrap.min.js"></script>
  <script src="js/datatables/dataTables.scroller.min.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script> 


  <!-- pace -->
  <script src="js/pace/pace.min.js"></script>
  <script>

    $(function () {
      $("#t_dob").datepicker({
      changeMonth: true, 
      changeYear: true, 
      dateFormat:"dd-mm-yy",
      yearRange: "-90:+00",
      maxDate: new Date()

    });
  });



    var handleDataTableButtons = function () {
      "use strict";
      0 !== $("#datatable-responsive").length && $("#datatable-responsive").DataTable({
        scrollX: true,
        keys: true,
        fixedHeader: true,
        dom: "Blfrtip",
        lengthMenu: [
        [5, 10, 25, 50, -1],
        [5, 10, 25, 50, "All"]
        ],
        buttons: [{
          extend: "copy",
          className: "btn-sm",
          exportOptions: {
            columns: [1, 2, 3, 4, 5, 6]
          }
        }, {
          extend: "csv",
          className: "btn-sm",
          exportOptions: {
            columns: [1, 2, 3, 4, 5, 6]
          }
        }, {
          extend: "excel",
          className: "btn-sm",
          exportOptions: {
            columns: [1, 2, 3, 4, 5, 6]
          }
        }, {
          extend: "pdf",
          className: "btn-sm",
          exportOptions: {
            columns: [1, 2, 3, 4, 5, 6]
          }
        }, {
          extend: "print",
          className: "btn-sm",
          exportOptions: {
            columns: [1, 2, 3, 4, 5, 6]
          }
        }],
      })
    },
    TableManageButtons = function () {
      "use strict";
      return {
        init: function () {
          handleDataTableButtons()
        }
      }
    }();
  </script>
  <script type="text/javascript">
    $(document).ready(function () {
      $('#datatable').dataTable();
      $('#datatable-keytable').DataTable({
        keys: true
      });
    /* $('#datatable-responsive').DataTable({
       keys: true,
       fixedHeader: true
     });*/
     $('#datatable-scroller').DataTable({
      ajax: "js/datatables/json/scroller-demo.json",
      deferRender: true,
      scrollY: 380,
      scrollCollapse: true,
      scroller: true
    });
     var table = $('#datatable-fixed-header').DataTable({
      fixedHeader: true
    });
   });
    TableManageButtons.init();
  </script>


</body>

</html>