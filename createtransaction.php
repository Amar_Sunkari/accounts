<?php 
session_start();

include 'header.php'; 

$usersql = "select * from user";

$userresult = mysqli_query($con, $usersql);

$costcentersql = "select * from costcenter";

$costcenterresult = mysqli_query($con, $costcentersql);

?>

<!-- page content -->

<link href="css/jquery-ui.css" rel="stylesheet">
<div class="right_col" role="main">
  <div class="">
    <div class="page-title">

    </div>
    <div class="clearfix"></div>
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Transaction Details</h2>
            
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <br />
            <form id="demo-form2" class="form-horizontal form-label-left"  action = "update.php"  method = "post">


              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Transaction type</label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <select class="form-control" required name="t_type" id = "t_type" onchange="typecallback()">

                    <option value="">Choose option</option>
                    <option value="payment">Payment</option>
                    <option value="receipt">Receipt</option>
                    <option value="internal_transfer">Contra</option>

                    </select>
                  </div>
                </div>

                <div class="form-group" id="t_type_payment" style="display: none;">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Payment type</label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <select class="form-control" name="t_type" id="t_type_payment_val">

                    <option value="">Choose option</option>
                    <option value="bill_payment">Bill Payment</option>
                    <option value="advance">Advance</option>
                    <option value="raas">Return against advance settlement</option>

                    </select>
                  </div>
                </div>

                <div class="form-group" id="t_type_receipt" style="display: none;">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Receipt type</label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <select class="form-control" name="t_type" id="t_type_receipt_val">

                    <option value="">Choose option</option>
                    <option value="donation">Donation</option>
                    <option value="raas">Return against advance settlement</option>
                    <option value="book_sale">Book Sale</option>
                    <option value="subscription">Subscription</option>

                    </select>
                  </div>
                </div>

               <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12">Description<span class="required"></span>
                      </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" id="t_detail" required="required" class="form-control col-md-7 col-xs-12" name="t_detail" maxlength="40">
                      </div>
                    </div>

               <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Mode</label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <select class="form-control" name="t_mode" id="t_mode" required>

                    <option value="">Choose option</option>
                    <option value="cash">Cash</option>
                    <option value="neft/online">NEFT/Online</option>
                    <option value="cheque">Cheque</option>

                    </select>
                  </div>
                </div>

              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">From Cost Center</label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <select class="form-control" required name="t_costcenter_id">
                    <option value="">Choose option</option>
                    <?php
                    while($costcenterrow = mysqli_fetch_assoc($costcenterresult)){
                      ?>
                      <option value="<?php echo $costcenterrow['c_id'] ;?>"><?php echo $costcenterrow['c_name'] ;?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>

                <div class="form-group" id="t_tocostcenter_id" style="display: none;">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">To Cost Center</label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <select class="form-control" name="t_tocostcenter_id" id="t_tocostcenter_id_val">
                    <option value="">Choose option</option>
                    <?php
                    $costcenterresult = mysqli_query($con, $costcentersql);
                    while($costcenterrow = mysqli_fetch_assoc($costcenterresult)){
                      ?>
                      <option value="<?php echo $costcenterrow['c_id'] ;?>"><?php echo $costcenterrow['c_name'] ;?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>

                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12">Giver/Receiver</label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <select class="form-control" required name="t_user_id">
                      <option value="">Choose option</option>
                      <?php

                      while($userrow = mysqli_fetch_assoc($userresult)){

                        ?>

                        <option value="<?php echo $userrow['u_id'] ;?>"><?php echo $userrow['u_name'] ;?></option>
                        <?php } ?>
                      </select>
                    </div>
                  </div>

                  <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="t_amount">Amount in Rs.<span class="required"></span>
                      </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="number" id="t_amount" name="t_amount" required="required" class="form-control col-md-7 col-xs-12">
                      </div>
                    </div>


                    <div class="ln_solid"></div>
                    <div class="form-group">
                      <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <button type="submit" class="btn btn-primary" name = "create_transaction">Create</button>

                      </div>
                    </div>

                  </form>
                </div>
              </div>
            </div>
          </div>





        </div>
        <div class="clearfix"></div>







        <!-- footer content -->

        <?php include 'footer.php';  ?>
        <!-- /footer content -->

      </div>
      <!-- /page content -->
    </div>

  </div>

  <div id="custom_notifications" class="custom-notifications dsp_none">
   <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
   </ul>
   <div class="clearfix"></div>
   <div id="notif-group" class="tabbed_notifications"></div>
 </div>

 <script src="js/bootstrap.min.js"></script>

 <!-- bootstrap progress js -->
 <script src="js/progressbar/bootstrap-progressbar.min.js"></script>
 <script src="js/nicescroll/jquery.nicescroll.min.js"></script>
 <!-- icheck -->
 <script src="js/icheck/icheck.min.js"></script>

 <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
 <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

 <script src="js/pace/pace.min.js"></script>
 <!-- <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>  -->
 <script>


   function typecallback(){

    var type = $('#t_type').val();

    if(type == 'payment') {
      $('#t_type_payment').show(100);
      $('#t_type_receipt').hide(100);
      $("#t_type_payment_val").prop('required',true);
      $("#t_type_receipt_val").prop('required',false);
      $('#t_tocostcenter_id').hide(100);
      $("#t_tocostcenter_id_val").prop('required',false);
    } else if(type == 'receipt') {
      $('#t_type_payment').hide(100);
      $('#t_type_receipt').show(100);
      $("#t_type_payment_val").prop('required',false);
      $("#t_type_receipt_val").prop('required',true);
      $('#t_tocostcenter_id').hide(100);
      $("#t_tocostcenter_id_val").prop('required',false);
    } else {
      $('#t_type_payment').hide(100);
      $('#t_type_receipt').hide(100);
      $("#t_type_payment_val").prop('required',false);
      $("#t_type_receipt_val").prop('required',false);

      if(type == 'internal_transfer'){
        $('#t_tocostcenter_id').show(100);
        $("#t_tocostcenter_id_val").prop('required',true);
      } else {
        $('#t_tocostcenter_id').hide(100);
        $("#t_tocostcenter_id_val").prop('required',false);
      }
    }

   }

</script>




</body>

</html>
