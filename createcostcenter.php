<?php 
session_start();


include 'header.php'; 

$accesssql = "select * from accesslevel";

$accessresult = mysqli_query($con, $accesssql);

$costcenterRow;

if(isset($_GET['editCostcenter'])) {

  $costcentersql = "select * from costcenter
                    inner join accesslevel
                    on c_a_id = accesslevel.a_id
                    where c_id = ".$_GET['editCostcenter'];

  $result = mysqli_query($con, $costcentersql);

  $costcenterRow = mysqli_fetch_assoc($result);

}


?>

<!-- page content -->

<link href="css/jquery-ui.css" rel="stylesheet">
<div class="right_col" role="main">
  <div class="">
    <div class="page-title">

    </div>
    <div class="clearfix"></div>
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Cost Center Details</h2>
            
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <br />
            <form id="demo-form2" class="form-horizontal form-label-left"  action = "update.php"  method = "get">

              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Cost Center Name<span class="required"></span>
                </label>
                <div class="col-md-3 col-sm-3 col-xs-12">
                  <input id="c_name" class="form-control col-md-7 col-xs-12" required="required" type="text" name = "c_name" autocomplete="off" value="<?php echo isset($_GET['editCostcenter'])?$costcenterRow['c_name']:''; ?>" >
                </div>
              </div>

              
              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Opening Balance<span class="required"></span>
                </label>
                <div class="col-md-3 col-sm-3 col-xs-12">
                  <input id="c_opening_balance" class="form-control col-md-7 col-xs-12" required="required" type="number" name = "c_opening_balance" autocomplete="off" value="<?php echo isset($_GET['editCostcenter'])?$costcenterRow['c_opening_balance']:''; ?>" >
                </div>
              </div>
                
              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Closing Balance<span class="required"></span>
                </label>
                <div class="col-md-3 col-sm-3 col-xs-12">
                  <input id="c_closing_balance" class="form-control col-md-7 col-xs-12" required="required" type="number" name = "c_closing_balance" autocomplete="off" value="<?php echo isset($_GET['editCostcenter'])?$costcenterRow['c_closing_balance']:''; ?>" >
                </div>
              </div>

              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Total Advance<span class="required"></span>
                </label>
                <div class="col-md-3 col-sm-3 col-xs-12">
                  <input id="c_advance" class="form-control col-md-7 col-xs-12" required="required" type="number" name = "c_advance" autocomplete="off" value="<?php echo isset($_GET['editCostcenter'])?$costcenterRow['c_advance']:''; ?>" >
                </div>
              </div>

              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">HOD Name<span class="required"></span>
                </label>
                <div class="col-md-3 col-sm-3 col-xs-12">
                  <input id="c_hod" class="form-control col-md-7 col-xs-12" required="required" type="text" name = "c_hod" autocomplete="off" value="<?php echo isset($_GET['editCostcenter'])?$costcenterRow['c_hod']:''; ?>" >
                </div>
              </div>
s
              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">HOD Mobile No. <span class="required"></span>
                </label>
                <div class="col-md-3 col-sm-3 col-xs-12">
                  <input id="c_hod_mobile" class="form-control col-md-7 col-xs-12" required="required" type="text" pattern="^\d{10}$" name = "c_hod_mobile" autocomplete="off" value="<?php echo isset($_GET['editCostcenter'])?$costcenterRow['c_hod_mobile']:''; ?>" >
                </div>
              </div>

              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">User Name<span class="required"></span>
                </label>
                <div class="col-md-3 col-sm-3 col-xs-12">
                  <input id="c_username" class="form-control col-md-7 col-xs-12" required="required" type="text" name = "c_username" autocomplete="off" value="<?php echo isset($_GET['editCostcenter'])?$costcenterRow['c_username']:''; ?>" >
                </div>
              </div>

              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Login Password<span class="required"></span>
                </label>
                <div class="col-md-3 col-sm-3 col-xs-12">
                  <input id="c_password" class="form-control col-md-7 col-xs-12" required="required" type="text" name = "c_password" autocomplete="off" value="<?php echo isset($_GET['editCostcenter'])?$costcenterRow['c_password']:''; ?>" >
                </div>
              </div>

              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Access Level</label>
                <div class="col-md-3 col-sm-3 col-xs-12">
                  <select class="form-control" required name="c_a_id">
                    <option value="">Choose option</option>

                    <?php

                    while($accessrow = mysqli_fetch_assoc($accessresult)){

                      ?>
                      
                      <option value="<?php echo $accessrow['a_id'] ;?>"  <?php echo (isset($_GET['editCostcenter']) && $costcenterRow['a_id']==$accessrow['a_id'])?'selected':''; ?> > <?php echo $accessrow['a_name'] ;?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>

                <input type="hidden" name="c_id" value="<?php echo isset($_GET['editCostcenter'])?$costcenterRow['c_id']:''; ?>" >

                    <div class="ln_solid"></div>
                    <div class="form-group">
                      <div class="col-md-3 col-sm-3 col-xs-12 col-md-offset-3">
                        <button type="submit" class="btn btn-primary" name = "<?php echo isset($_GET['editCostcenter'])?'updateCostCenter':'saveCostCenter'; ?>">Save</button>

                      </div>
                    </div>

                  </form>
                </div>
              </div>
            </div>
          </div>





        </div>
        <div class="clearfix"></div>







        <!-- footer content -->

        <?php include 'footer.php';  ?>
        <!-- /footer content -->

      </div>
      <!-- /page content -->
    </div>

  </div>

  <div id="custom_notifications" class="custom-notifications dsp_none">
   <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
   </ul>
   <div class="clearfix"></div>
   <div id="notif-group" class="tabbed_notifications"></div>
 </div>

 <script src="js/bootstrap.min.js"></script>

 <!-- bootstrap progress js -->
 <script src="js/progressbar/bootstrap-progressbar.min.js"></script>
 <script src="js/nicescroll/jquery.nicescroll.min.js"></script>
 <!-- icheck -->
 <script src="js/icheck/icheck.min.js"></script>

 <script src="js/custom.js"></script>

 <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
 <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>



 <script src="js/pace/pace.min.js"></script>
 <script>


    document.addEventListener("DOMContentLoaded", function() {

         var elements1 = document.querySelector('input[name="c_hod_mobile"]');
        elements1.oninvalid = function(e) {
            e.target.setCustomValidity("");
            if (!e.target.validity.valid) {
                e.target.setCustomValidity("Please enter a valid 10 digit Mobile No.");
            }
        };
        elements1.oninput = function(e) {
            e.target.setCustomValidity("");
        };
})

</script>




</body>

</html>
