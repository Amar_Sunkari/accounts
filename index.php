<?php
session_start();
date_default_timezone_set('Asia/Kolkata');
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <!-- Meta, title, CSS, favicons, etc. -->
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title>Login | Secure Accounting</title>

  <!-- Bootstrap core CSS -->

  <link href="css/bootstrap.min.css" rel="stylesheet">

  <link href="fonts/css/font-awesome.min.css" rel="stylesheet">
  <link href="css/animate.min.css" rel="stylesheet">

  <!-- Custom styling plus plugins -->
  <link href="css/custom.css" rel="stylesheet">
  <link href="css/icheck/flat/green.css" rel="stylesheet">



  <script src="js/jquery.min.js"></script>


  <!--[if lt IE 9]>
        <script src="../assets/js/ie8-responsive-file-warning.js"></script>
      <![endif]-->

      <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

      </head>

      <body style="background:#F7F7F7;">



        <div class="">
          <a class="hiddenanchor" id="toregister"></a>
          <a class="hiddenanchor" id="tologin"></a>


          



         <?php 
          if(isset($_SESSION['loggedin'])){

              ?>
              
            
              <script type="text/javascript">  window.location.href = 'donations.php';  </script>



          <?php
            
              
           

          }


          if(isset($_GET['loginerror']) && $_GET['loginerror']=='true'){


            ?>
            <div class="alert alert-danger alert-dismissible fade in" role="alert" align = "center">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
              </button> 
              
            <?php
              if(isset($_GET['forgotpassword'])){
                echo "<strong >User Id Not Found, Please Enter Correct User Id</strong>";
              } else {
                echo "<strong >Incorrect Username or Password! Please login again</strong>";
              }
              
            echo "</div>";


          }

          if (!isset($_GET['forgotpassword'])) {

          ?>
          <div id="wrapper">

            <div id="user" class="animate form">
              <section class="login_content">
                <form method = "post" action = "update.php">
                  <h1>Login Details</h1>
                  <div>
                    <input type="text" class="form-control" placeholder="User Name" name="c_username" id="c_username" required />
                  </div>
                  <br/>
                  <div>
                    <input type="password" class="form-control" placeholder="Password" name = "c_password" required="" />
                  </div>
                  <div>
                    <input type="submit" class="btn btn-default submit" name = "userlogin" value = "Login"/>
                    <a class="reset_pass" href="index.php?forgotpassword=true#toregister">Lost your password?</a>
                  </div>
                  <div class="clearfix"></div>
                  <div class="separator">

                    <!-- <p class="change_link">
                      <a href="#tologin" class="to_register">Go to Student Login</a>
                    </p> -->
                    <div class="clearfix"></div>
                    <br />
                    <div>
                     <h1>Secure Accounting</h1>
                     <p>Accounting Simplified</p>

                     <p>©<?php echo date('Y') ?> All Rights Reserved</p>
                   </div>
                 </div>
               </form>
               <!-- form -->
             </section>
             <!-- content -->
           </div>
         </div>

         <?php 
          } else { 
         ?>
          <div id="wrapper">
            <div id="register" class="animate form">
              <section class="login_content">
                <form method = "post" action = "update.php">
                  <h1>Password Recovery</h1>
                  <?php
                      if (isset($_GET['newpassword'])) {
                  ?>

                  <div>
                    <input type="password" class="form-control" placeholder="New Password" name="c_password" required/>
                  </div>

                  <div>
                    <input type="password" class="form-control" placeholder="Re-Enter Password" name="c_password_confirm" required/>
                  </div>

                  <input type = "hidden" name = "c_id" value = "<?php echo ($_GET['token']/1975);?>">

                  <div>
                    <input type="submit" class="btn btn-default submit" name = "costcenter_update_password" value = "Next"/>
                  </div>

                  
                  
                  <?php
                      } else {

                  ?>
                  <div>
                    <input type="text" class="form-control" placeholder="User Id" name="c_username" required="" />
                  </div>
                  <div>
                    <input type="submit" class="btn btn-default submit" name = "costcenter_forgotpassword" value = "Next"/>
                  </div>

                  <?php
                      } 

                  ?>
                  <div class="clearfix"></div>
                  <div class="separator">

                    <div class="clearfix"></div>
                    <br />
                    <div>
                     <h1>Arjuna Educational Institutions</h1>
                     <p>Where Personal Care is the Culture</p>

                     <p>©<?php echo date('Y') ?> All Rights Reserved</p>
                   </div>
                 </div>
               </form>
               <!-- form -->
             </section>
             <!-- content -->
           </div>
         </div>

         <?php

          }

         ?>

       </div>
       <script src="js/bootstrap.min.js"></script>

       <!-- bootstrap progress js -->
       <script src="js/progressbar/bootstrap-progressbar.min.js"></script>
       <script src="js/nicescroll/jquery.nicescroll.min.js"></script>
       <!-- icheck -->
       <script src="js/icheck/icheck.min.js"></script>

       <script src="js/custom.js"></script>
       <!-- pace -->
       <script src="js/pace/pace.min.js"></script>
       <!-- PNotify -->
       <script type="text/javascript" src="js/notify/pnotify.core.js"></script>
       <script type="text/javascript" src="js/notify/pnotify.buttons.js"></script>
       <script type="text/javascript" src="js/notify/pnotify.nonblock.js"></script>


     </body>

     </html>
