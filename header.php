
<?php

if( !isset($_SESSION['loggedin']))
  header('Location: index.php');


include 'connect.php';



?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <!-- Meta, title, CSS, favicons, etc. -->
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title>Create Receipt</title>

  <!-- Bootstrap core CSS -->
  <link href="css/jquery-ui.css" rel="stylesheet">

  <link href="css/bootstrap.min.css" rel="stylesheet">

  <link href="https://colorlib.com/polygon/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="css/animate.min.css" rel="stylesheet">

  <!-- Custom styling plus plugins -->
  <link href="css/custom.css" rel="stylesheet">
  <link href="css/icheck/flat/green.css" rel="stylesheet">

  <link href="js/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
  <link href="js/datatables/buttons.bootstrap.min.css" rel="stylesheet" type="text/css" />
  <link href="js/datatables/fixedHeader.bootstrap.min.css" rel="stylesheet" type="text/css" />
  <link href="js/datatables/responsive.bootstrap.min.css" rel="stylesheet" type="text/css" />
  <link href="js/datatables/scroller.bootstrap.min.css" rel="stylesheet" type="text/css" />

  <script src="js/jquery.min.js"></script>

  <!--[if lt IE 9]>
        <script src="../assets/js/ie8-responsive-file-warning.js"></script>
      <![endif]-->

      <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

      </head>


      <body class="nav-md">

        <div class="container body">


          <div class="main_container">

           <div class="col-md-3 left_col">
            <div class="left_col scroll-view">

              <div class="navbar nav_title" style="border: 0;">
                <a href="index.html" class="site_title"><i class="fa fa-paw"></i> <span>Secure Accounts</span></a>
              </div>
              <div class="clearfix"></div>

              <!-- menu prile quick info -->
              <div class="profile">
                <div class="profile_pic">
                  <img src="images/img.jpg" alt="..." class="img-circle profile_img">
                </div>
                <div class="profile_info">
                  <span>Welcome,</span>
                  <h2><?php echo $_SESSION['loggedin']['c_hod'].'<br>('.$_SESSION['loggedin']['a_name'].')' ?></h2>
                </div>
              </div>
              <!-- /menu prile quick info -->

              <br />

              <!-- sidebar menu -->
              <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">

                <div class="menu_section">
                  <h3> &nbsp </h3>
                  <ul class="nav side-menu">
<!-- 
                   <li><a><i class="fa fa-group"></i> Student Database <span class="fa fa-chevron-down"></span></a>
                  <ul class="nav child_menu" style="display: none">
                    <li><a href="student_list.php">Student Master List</a>
                    </li>
                    <li><a href="upload_students.php">Bulk Upload Students</a>
                    </li>
                    <li><a href="upload_attendance.php">Upload Attendance</a>
                    </li>
                  </ul>
                </li>  -->

                  <li>
                    <a href = "createreceipt.php"><i class="fa fa-plus-square"></i> Create Receipt </span></a>

                  </li>

                  <li>
                    <a href = "donations.php"><i class="fa fa-tasks"></i> Donation History </span></a>

                  </li>
                  
                  <li <?php echo ($_SESSION['loggedin']['a_id']==3)?"style='display:none'":"" ?> >
                    <a href = "costcenterstatement.php"><i class="fa fa-tasks"></i> CostCenter Statement </span></a>

                  </li>

                  <li>
                    <a href = "costcenters.php"><i class="fa fa-tasks"></i> CostCenter List </span></a>

                  </li>
                  
                <!--   <?php if($_SESSION['isadmin']){ ?>

                  <li>
                    <a href = "users.php"><i class="fa fa-list-ul"></i> Users </span></a>

                  </li>
                  <li>
                    <a href = "costcenters.php"><i class="fa fa-list-ul"></i> Cost Centers </span></a>

                  </li>

                  <?php } ?> -->


                </ul>
              </li>

            </ul>
          </div>

        </div>
        <!-- /sidebar menu -->

        <!-- /menu footer buttons -->
<!--               <div class="sidebar-footer hidden-small">
                <a data-toggle="tooltip" data-placement="top" title="Settings">
                  <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
                </a>
                <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                  <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
                </a>
                <a data-toggle="tooltip" data-placement="top" title="Lock">
                  <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
                </a>
                <a data-toggle="tooltip" data-placement="top" title="Logout" href = "update.php?logout=teacher">
                  <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
                </a>
              </div> -->
              <!-- /menu footer buttons -->
            </div>
          </div>

          <!-- top navigation -->
          <div class="top_nav">

            <div class="nav_menu">
              <nav class="" role="navigation">
                <div class="nav toggle">
                  <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <img src="images/img.jpg" alt=""><?php echo $_SESSION['loggedin']['c_hod']; ?>
                    <span class=" fa fa-angle-down"></span>
                  </a>
                  <ul class="dropdown-menu dropdown-usermenu animated fadeInDown pull-right">
                    <li><a href="javascript:;">  Profile</a>
                    </li>
                    <li><a href="update.php?logout=true"><i class="fa fa-sign-out pull-right"></i> Log Out</a>
                    </li>
                  </ul>
                </li>

<!--                 <li role="presentation" class="dropdown">
                  <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
                    <i class="fa fa-envelope-o"></i>
                    <span class="badge bg-green">6</span>
                  </a>
                  <ul id="menu1" class="dropdown-menu list-unstyled msg_list animated fadeInDown" role="menu">
                    <li>
                      <a>
                        <span class="image">
                          <img src="images/img.jpg" alt="Profile Image" />
                        </span>
                        <span>
                          <span>John Smith</span>
                          <span class="time">3 mins ago</span>
                        </span>
                        <span class="message">
                          Film festivals used to be do-or-die moments for movie makers. They were where...
                        </span>
                      </a>
                    </li>
                    <li>
                      <a>
                        <span class="image">
                          <img src="images/img.jpg" alt="Profile Image" />
                        </span>
                        <span>
                          <span>John Smith</span>
                          <span class="time">3 mins ago</span>
                        </span>
                        <span class="message">
                          Film festivals used to be do-or-die moments for movie makers. They were where...
                        </span>
                      </a>
                    </li>
                    <li>
                      <a>
                        <span class="image">
                          <img src="images/img.jpg" alt="Profile Image" />
                        </span>
                        <span>
                          <span>John Smith</span>
                          <span class="time">3 mins ago</span>
                        </span>
                        <span class="message">
                          Film festivals used to be do-or-die moments for movie makers. They were where...
                        </span>
                      </a>
                    </li>
                    <li>
                      <a>
                        <span class="image">
                          <img src="images/img.jpg" alt="Profile Image" />
                        </span>
                        <span>
                          <span>John Smith</span>
                          <span class="time">3 mins ago</span>
                        </span>
                        <span class="message">
                          Film festivals used to be do-or-die moments for movie makers. They were where...
                        </span>
                      </a>
                    </li>
                    <li>
                      <div class="text-center">
                        <a>
                          <strong>See All Alerts</strong>
                          <i class="fa fa-angle-right"></i>
                        </a>
                      </div>
                    </li>
                  </ul>
                </li>
              -->
            </ul>
          </nav>
        </div>

      </div>
              <!-- /top navigation -->