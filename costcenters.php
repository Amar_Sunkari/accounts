<?php 

session_start();

include 'header.php';


?>

<!-- page content -->

<style type="text/css">
.modal-dialog{
  overflow-y: initial !important
}
.modal-body{
  height: 300px;
  overflow-y: auto;
}

</style>
<link href="css/jquery-ui.css" rel="stylesheet">

<div class="right_col" role="main">
  <div class="">
    <div class="page-title">

    </div>
    <div class="clearfix"></div>



  </div>
  <div class="clearfix"></div>



  <div class="row">

    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h2>Cost Centers<small></small></h2>

          <div class="clearfix"></div>
        </div>
        <div class="x_content">

          <table id="datatable-responsive" class="table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
              <tr>
                <th>Sl No</th>
                <th <?php echo ($_SESSION['loggedin']['a_id']==1)?"":"style='display:none'" ?> >Update</th>
                <th>Name</th>
                <th>Username</th>
                <th>Opening Balance</th>
                <th>Closing Balance</th>
                <th>Total Advance</th>
                <th>HOD</th>
                <th>HOD Mobile No</th>
                <th>Password</th>
                <th>Access Role</th>
              </tr>
            </thead>
            <tbody>

              <?php



              $costcentersql = "select * from costcenter
              inner join accesslevel
              on c_a_id = accesslevel.a_id";
              

              $result = mysqli_query($con, $costcentersql);

              $slno=0;

              while ($costcenterrow = mysqli_fetch_assoc($result)) {
                $slno++;


                ?>

                <tr>

                   <td>
                    <?php echo $slno ?>
                  </td>
                  <td <?php echo ($_SESSION['loggedin']['a_id']==1)?"":"style='display:none'" ?>  >
                    <a href="createcostcenter.php?editCostcenter=<?php echo $costcenterrow['c_id'] ?>"><i class="glyphicon glyphicon-edit" style="font-size: 22px"></i></a>
                  </td>
                  <td>
                    <?php echo $costcenterrow['c_name'] ?>
                  </td>
                  <td>
                    <?php echo $costcenterrow['c_username'] ?>
                  </td>
                   <td>
                    <?php echo ($costcenterrow['c_opening_balance']) ?>
                  </td>
                  <td>
                    <?php echo $costcenterrow['c_closing_balance'] ?>
                  </td>
                  <td>
                    <?php echo $costcenterrow['c_advance'] ?>
                  </td>
                  <td>
                    <?php echo $costcenterrow['c_hod'] ?>
                  </td>
                  <td>
                    <?php echo $costcenterrow['c_hod_mobile'] ?>
                  </td>
                  <td>
                    <?php echo $costcenterrow['c_password'] ?>
                  </td>
                  <td>
                    <?php echo $costcenterrow['a_name'] ?>
                  </td>
                  </tr>

                  <?php

                }
                ?>
              </tbody>
            </table>


          </div>
        </div>
      </div>
    </div>

    <div class="col-md-3 col-sm-12 col-xs-12" style="margin-bottom: 50px;margin-top: 10px;" >

      <a <?php echo ($_SESSION['loggedin']['a_id']==1)?"":"style='display:none'" ?> class="btn btn-warning" href="createcostcenter.php">Create Cost Center</a>


      </div>







    <div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-hidden="true" id="comment_modal">
      <div class="modal-dialog modal-sm">
        <div class="modal-content">


        </div>
      </div>
    </div>





    <!-- footer content -->

    <?php include 'footer.php'; ?>
    <!-- /footer content -->

  </div>
  <!-- /page content -->
</div>

</div>

<div id="custom_notifications" class="custom-notifications dsp_none">
  <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
  </ul>
  <div class="clearfix"></div>
  <div id="notif-group" class="tabbed_notifications"></div>
</div>

<script src="js/bootstrap.min.js"></script>

<!-- bootstrap progress js -->
<script src="js/progressbar/bootstrap-progressbar.min.js"></script>
<script src="js/nicescroll/jquery.nicescroll.min.js"></script>
<!-- icheck -->
<script src="js/icheck/icheck.min.js"></script>

<script src="js/custom.js"></script>



<!-- Datatables -->
<!-- <script src="js/datatables/js/jquery.dataTables.js"></script>
  <script src="js/datatables/tools/js/dataTables.tableTools.js"></script> -->

  <!-- Datatables-->
  <!-- <script src="js/jquery.min.js"></script> -->
  <script src="js/datatables/jquery.dataTables.min.js"></script>
  <script src="js/datatables/dataTables.bootstrap.js"></script>
  <script src="js/datatables/dataTables.buttons.min.js"></script>
  <script src="js/datatables/buttons.bootstrap.min.js"></script>
  <script src="js/datatables/jszip.min.js"></script>
  <script src="js/datatables/pdfmake.min.js"></script>
  <script src="js/datatables/vfs_fonts.js"></script>
  <script src="js/datatables/buttons.html5.min.js"></script>
  <script src="js/datatables/buttons.print.min.js"></script>
  <script src="js/datatables/dataTables.fixedHeader.min.js"></script>
  <script src="js/datatables/dataTables.keyTable.min.js"></script>
  <script src="js/datatables/dataTables.responsive.min.js"></script>
  <script src="js/datatables/responsive.bootstrap.min.js"></script>
  <script src="js/datatables/dataTables.scroller.min.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script> 


  <!-- pace -->
  <script src="js/pace/pace.min.js"></script>
  <script>

    <?php  

    if(isset($_GET['addcostcenter'])){
      echo "alert('Cost Center Added Successfully');";
    }

    if(isset($_GET['editcostcenter'])){
      echo "alert('Cost Center Updated Successfully');";
    }

    ?>

    $(function () {
      $("#t_dob").datepicker({
      changeMonth: true, 
      changeYear: true, 
      dateFormat:"dd-mm-yy",
      yearRange: "-90:+00",
      maxDate: new Date()

    });
  });



    var handleDataTableButtons = function () {
      "use strict";
      0 !== $("#datatable-responsive").length && $("#datatable-responsive").DataTable({
        scrollX: true,
        keys: true,
        fixedHeader: true,
        dom: "Blfrtip",
        lengthMenu: [
        [5, 10, 25, 50, -1],
        [5, 10, 25, 50, "All"]
        ],
        buttons: [{
          extend: "copy",
          className: "btn-sm",
          exportOptions: {
            columns: [1, 2, 3, 4, 5, 6, 7]
          }
        }, {
          extend: "csv",
          className: "btn-sm",
          exportOptions: {
            columns: [1, 2, 3, 4, 5, 6, 7]
          }
        }, {
          extend: "excel",
          className: "btn-sm",
          exportOptions: {
            columns: [1, 2, 3, 4, 5, 6, 7]
          }
        }, {
          extend: "pdf",
          className: "btn-sm",
          exportOptions: {
            columns: [1, 2, 3, 4, 5, 6, 7]
          }
        }, {
          extend: "print",
          className: "btn-sm",
          exportOptions: {
            columns: [1, 2, 3, 4, 5, 6, 7]
          }
        }],
      })
    },
    TableManageButtons = function () {
      "use strict";
      return {
        init: function () {
          handleDataTableButtons()
        }
      }
    }();
  </script>
  <script type="text/javascript">
    $(document).ready(function () {
      $('#datatable').dataTable();
      $('#datatable-keytable').DataTable({
        keys: true
      });
    /* $('#datatable-responsive').DataTable({
       keys: true,
       fixedHeader: true
     });*/
     $('#datatable-scroller').DataTable({
      ajax: "js/datatables/json/scroller-demo.json",
      deferRender: true,
      scrollY: 380,
      scrollCollapse: true,
      scroller: true
    });
     var table = $('#datatable-fixed-header').DataTable({
      fixedHeader: true
    });
   });
    TableManageButtons.init();
  </script>


</body>

</html>