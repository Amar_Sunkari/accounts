<?php

$con=mysqli_connect("localhost","root","","accounts");

// Check connection

if (mysqli_connect_errno()) {
	echo "Failed to connect to MySQL: " . mysqli_connect_error();
}

//Utility Functions

function convertdatedb($date){
	
	$converted_date = substr($date, 6,4)."-".substr($date, 3,2)."-".substr($date, 0,2);

	return $converted_date;
}


function convertdatenormal($date){
	
	$converted_date = substr($date, 8,2)."-".substr($date, 5,2)."-".substr($date, 0,4);

	return $converted_date;
}

function getIndianCurrency(float $number)
{
    $decimal = round($number - ($no = floor($number)), 2) * 100;
    $hundred = null;
    $digits_length = strlen($no);
    $i = 0;
    $str = array();
    $words = array(0 => '', 1 => 'one', 2 => 'two',
        3 => 'three', 4 => 'four', 5 => 'five', 6 => 'six',
        7 => 'seven', 8 => 'eight', 9 => 'nine',
        10 => 'ten', 11 => 'eleven', 12 => 'twelve',
        13 => 'thirteen', 14 => 'fourteen', 15 => 'fifteen',
        16 => 'sixteen', 17 => 'seventeen', 18 => 'eighteen',
        19 => 'nineteen', 20 => 'twenty', 30 => 'thirty',
        40 => 'forty', 50 => 'fifty', 60 => 'sixty',
        70 => 'seventy', 80 => 'eighty', 90 => 'ninety');
    $digits = array('', 'hundred','thousand','lakh', 'crore');
    while( $i < $digits_length ) {
        $divider = ($i == 2) ? 10 : 100;
        $number = floor($no % $divider);
        $no = floor($no / $divider);
        $i += $divider == 10 ? 1 : 2;
        if ($number) {
            // $plural = (($counter = count($str)) && $number > 9) ? 's' : null;
            $plural = (($counter = count($str)) && $number > 9) ? '' : null;
            $hundred = ($counter == 1 && $str[0]) ? 'and ' : null;
            $str [] = ($number < 21) ? $words[$number].' '. $digits[$counter]. $plural.' '.$hundred:$words[floor($number / 10) * 10].' '.$words[$number % 10]. ' '.$digits[$counter].$plural.' '.$hundred;
        } else $str[] = null;
    }
    $Rupees = implode('', array_reverse($str));
    $paise = ($decimal > 0) ? "." . ($words[$decimal / 10] . " " . $words[$decimal % 10]) . ' Paise' : '';
    return ($Rupees ? $Rupees . '' : '') . $paise . 'only';
}



function moneyFormatIndia($num) {
		$explrestunits = "";
		if (strlen($num) > 3) {
		$lastthree = substr($num, strlen($num) - 3, strlen($num));
		$restunits = substr($num, 0, strlen($num) - 3);
		$restunits = (strlen($restunits) % 2 == 1) ? "0" . $restunits : $restunits;
		$expunit = str_split($restunits, 2);
		for ($i = 0; $i < sizeof($expunit); $i++) {
		if ($i == 0) {
		$explrestunits .= (int) $expunit[$i] . ",";
		} else {
		$explrestunits .= $expunit[$i] . ",";
		}
		}
		$thecash = $explrestunits . $lastthree;
		} else {
		$thecash = $num;
		}
		return $thecash;
}


function sendSMS($mobileNumbers,$smsmessage) {
    $authKey = "18386AcpBEnDEpL603cf033P15";
//Multiple mobiles numbers separated by comma
    // $mobileNumbers =  $programrow['confirmationmobileno'];
//Sender ID,While using route4 sender id should be 6 characters long.
    $senderId = "ISKCON";
//Your message to send, Add URL encoding here.
    $message = urlencode( $smsmessage);
//Define route 
    $route = 4;
//Prepare you post parameters
    $postData = array(
        'authkey' => $authKey,
        'mobiles' => $mobileNumbers,
        'message' => $message,
        'sender' => $senderId,
        'route' => $route
    );
//API URL
    $url="http://login.bulksms.bz/api/sendhttp.php";
// init the resource
    $ch = curl_init();
    curl_setopt_array($ch, array(
        CURLOPT_URL => $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_POST => true,
        CURLOPT_POSTFIELDS => $postData
        //,CURLOPT_FOLLOWLOCATION => true
    ));
//Ignore SSL certificate verification
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
//get response
    $output = curl_exec($ch);
//Print error if any
    if(curl_errno($ch))
    {
        echo 'error:' . curl_error($ch);
    }
    curl_close($ch);
    // header('Location: registrationsuccess.php');
}
?>