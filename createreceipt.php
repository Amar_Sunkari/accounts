<?php 
session_start();

// $page='addnewtimesheet.php';

include 'header.php'; 

$costcentersql = "select * from costcenter";

$costcenterresult = mysqli_query($con, $costcentersql);

$banksql = "select * from bank";

$bankresult = mysqli_query($con, $banksql);

$t_donor_address;

$disabled = false;
$checkMessage = '<b>';

if(isset($_GET['editReceipt'])) {

  $receiptsql = "select * from transaction where t_id = ".$_GET['editReceipt'];

  $result = mysqli_query($con, $receiptsql);

  $receiptRow = mysqli_fetch_assoc($result);

  $t_donor_address = explode('|', $receiptRow['t_donor_address']);

  

  

} else {

  

    $checkSql = "select count(*) as total from transaction where t_from_costcenter_id = ".$_SESSION['loggedin']['c_id']." and t_approve_status = 0";

    $result = mysqli_query($con, $checkSql);

    $checkRow = mysqli_fetch_assoc($result);

    $unapprovedReceipts1 = $checkRow['total'];

    $checkSql = "select count(*) as total from transaction where t_from_costcenter_id = ".$_SESSION['loggedin']['c_id']." and t_approve_status = 0 and t_date < ".date('Y-m-d', strtotime('-10 days')) ;

    // echo $checkSql;

    $result = mysqli_query($con, $checkSql);

    $checkRow = mysqli_fetch_assoc($result);

    $unapprovedReceipts2 = $checkRow['total'];

    if(($unapprovedReceipts1 >= 10 || $unapprovedReceipts2 > 0) && $_SESSION['loggedin']['a_id'] != 1) {
      $disabled = true;
    }



    $checkMessage = $checkMessage.$unapprovedReceipts1." receipts are yet to be approved.<br>";

    $checkMessage = $checkMessage.$unapprovedReceipts2." receipts haven't been approved for more than 10 days.</b><br>";

    $checkMessage =  $checkMessage."Cost Centers with more than 50 unapproved receipts or with any receipt unapproved for more than 10 days; can't create new receipts. Please try any of the below:<br>
  1. Please reuse any redundant unapproved receipts to make new receipts. <br>
  2. Pls provide necessary information to accounts dept. to clear the unapproved receipts. <br>" ;

}

// $classsql = "select * from class";

// $classresult = mysqli_query($con, $classsql);


?>

<!-- page content -->
<link href="css/jquery-ui.css" rel="stylesheet">
<div class="right_col" role="main">
  <div class="">
    <div class="page-title">

    </div>
    <div class="clearfix"></div>

    <?php if($disabled) {
      ?>



    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <h5><?php echo $checkMessage; ?></h5>
        </div>
      </div>
    </div>

  <? }
  ?>

<div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Donation Receipt</h2>
            
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <br />
            <form id="demo-form2" class="form-horizontal form-label-left"  action = "update.php"  method = "get" onsubmit="return(validateReceipt())">

               <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Receipt Type</label>
                <div class="col-md-3 col-sm-3 col-xs-12">
                  <select class="form-control" required name="t_type_id" id="t_type_id">
                    <option value="1" >Non 80G</option>
                    <option value="2" >80G</option>

                    </select>
                  </div>
                </div>

           

              <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="t_donor_name">Donor Name *
                      </label>
                      <div class="col-md-3 col-sm-3 col-xs-12">
                        <input type="text" id="t_donor_name" required class="form-control col-md-7 col-xs-12" name="t_donor_name" value="<?php echo isset($_GET['editReceipt'])?$receiptRow['t_donor_name']:'' ?>" >
                      </div>
                    </div>

              <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="donor_name">Donor Address *
                      </label>
                    </div>      

              <div class="form-group">
                       <label class="control-label col-md-3 col-sm-3 col-xs-12" for="donor_name">Flat, House no., Building *
                      </label>
                      <div class="col-md-3 col-sm-3 col-xs-12">
                        <input type="text" id="donor_address_1" required class="form-control col-md-7 col-xs-12" name="t_donor_address_1" value="<?php echo isset($_GET['editReceipt'])?$t_donor_address[0]:'' ?>" >
                      </div>
                    </div>   

               <div class="form-group">
                       <label class="control-label col-md-3 col-sm-3 col-xs-12" for="donor_name">Area, Colony, Street, Sector, Village *
                      </label>
                      <div class="col-md-3 col-sm-3 col-xs-12">
                        <input type="text" id="donor_address_2" required class="form-control col-md-7 col-xs-12" name="t_donor_address_2" value="<?php echo isset($_GET['editReceipt'])?$t_donor_address[1]:'' ?>" >
                      </div>
                    </div>     

              <div class="form-group">
                       <label class="control-label col-md-3 col-sm-3 col-xs-12" for="donor_name">Town/City *
                      </label>
                      <div class="col-md-3 col-sm-3 col-xs-12">
                        <input type="text" id="donor_address_3" required class="form-control col-md-7 col-xs-12" name="t_donor_address_3" value="<?php echo isset($_GET['editReceipt'])?$t_donor_address[2]:'' ?>" >
                      </div>
                    </div>    

               <div class="form-group">
                       <label class="control-label col-md-3 col-sm-3 col-xs-12" for="donor_name">PIN code *
                      </label>
                      <div class="col-md-3 col-sm-3 col-xs-12">
                        <input type="number" id="donor_address_4" required class="form-control col-md-7 col-xs-12" name="t_donor_address_4" min="100000" max="999999" value="<?php echo isset($_GET['editReceipt'])?$t_donor_address[3]:'' ?>" >
                      </div>
                    </div>   

                <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Payment Mode</label>
                <div class="col-md-3 col-sm-3 col-xs-12">
                  <select class="form-control" required name="t_mode" id="t_mode" onchange="modeChanged()">
                    <option value="cash">Cash</option>
                    <option value="online">Online</option>
                    <option value="cheque">Cheque</option>
                    </select>
                  </div>
                </div>  

              <div class="form-group" id="donationAmountCashCheque">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="donation_amount">Donation Amount *
                      </label>
                      <div class="col-md-3 col-sm-3 col-xs-12">
                        <input type="number" id="donation_amount" class="form-control col-md-7 col-xs-12" name="donation_amount">
                      </div>
                    </div>
              

                <div class="form-group" id = "chequeDetails">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="cheque_no">Cheque No. *
                      </label>
                      <div class="col-md-3 col-sm-3 col-xs-12">
                        <input type="text" id="cheque_no" class="form-control col-md-7 col-xs-12" name="t_cheque_no">
                      </div>
                    </div>


                <div id="paymentDetailsGroup">
                <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Online Payment Details</label>
                </div> 

              

              <div class="form-group" id = "abc">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" >Payment <span class='paymentslno'>1</span> *
                      </label>
                      <div class="col-md-3 col-sm-3 col-xs-12">
                        <input type="number" class="form-control col-md-7 col-xs-12 donationAmount" name="donationAmount[]" placeholder="Donation Amount" onkeyup="getTotalAmount()">
                      </div>
                      <div class="col-md-3 col-sm-3 col-xs-12">
                      <input class="form-control col-md-7 col-xs-12 paymentDate" id = 'dateabc' type="text" name = "paymentDate[]" autocomplete="off" placeholder="Donation Date">
                      </div>
                      <div class="col-md-3 col-sm-3 col-xs-12 removeElementButton">
                        <a class="btn btn-danger" onclick="removeElement('abc')"><i class="glyphicon glyphicon-trash"></i></a>
                      </div>
                    </div>  
                    <div id='eopd'></div>

                    <div class="form-group" >
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" >Total Receipt Amount
                      </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <h4><b>Rs. <span id='totalDonationAmount'>0</span></b></h4>
                      </div>
                    </div>


                    <div class="form-group" >
                      <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <a class="btn btn-primary" name = "add_amount" onclick="addElement()"> Add Another Amount </a>

                      </div>
                    </div>

                <div class="form-group" id="partialClaimSection">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Is the receipt being made as a partial claim of an Online donation?</label>
                <div class="col-md-3 col-sm-3 col-xs-12">
                  <select class="form-control" required name="t_is_part" id="partialClaim" onchange="isPartialClaim()">
                    <option value="0">No</option>
                    <option value="1">Yes</option>
                    </select>
                  </div>

                  <div class="col-md-3 col-sm-3 col-xs-12" id="actualDonationAmount">
                        <input type="number" id="actual_amount" class="form-control col-md-7 col-xs-12" name="t_actual_amount" placeholder="Pls Enter Actual Donation Amount">
                      </div>

                </div>

              </div>  

              <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="pan_no">PAN No.
                      </label>
                      <div class="col-md-3 col-sm-3 col-xs-12">
                        <input type="text" id="pan_no" class="form-control col-md-7 col-xs-12" name="t_donor_pan" value="<?php echo isset($_GET['editReceipt'])?$receiptRow['t_donor_pan']:'' ?>" >
                      </div>
                    </div> 

              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Deposited/ Yet to be deposited in Bank*</label>
                <div class="col-md-3 col-sm-3 col-xs-12">
                  <select class="form-control" required name="t_bank_id" id="t_bank_id">
                    <option value="">Select Bank</option>

                    <?php

                    while($bankrow = mysqli_fetch_assoc($bankresult)){

                      ?>
                      
                      <option value="<?php echo $bankrow['b_id'] ;?>" <?php echo ((isset($_GET['editReceipt']) && $receiptRow['t_bank_id']==$bankrow['b_id'])?'selected':'')?> > <?php echo $bankrow['b_name'] .' - ' . $bankrow['b_account_no'] ;?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>                    

              <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="mobile_no">Donor Mobile No.
                      </label>
                      <div class="col-md-3 col-sm-3 col-xs-12">
                        <input type="text" pattern="^\d{10}$" id="mobile_no" class="form-control col-md-7 col-xs-12" name="t_donor_mobile" value="<?php echo isset($_GET['editReceipt'])?$receiptRow['t_donor_mobile']:'' ?>" >
                      </div>
                    </div> 

              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Claiming CostCenter *</label>
                <div class="col-md-3 col-sm-3 col-xs-12">
                  <select class="form-control" required name="t_to_costcenter_id">
                    <option value="">Select</option>

                    <?php

                    while($costcenterrow = mysqli_fetch_assoc($costcenterresult)){

                      ?>
                      
                      <option value="<?php echo $costcenterrow['c_id'] ;?>" <?php echo ((isset($_GET['editReceipt']) && $receiptRow['t_to_costcenter_id']==$costcenterrow['c_id'])?'selected':'')?> > <?php echo $costcenterrow['c_name'] ;?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>    


              <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="receipt_made_by">Receipt Being Made By *
                      </label>
                      <div class="col-md-3 col-sm-3 col-xs-12">
                        <input type="text" id="t_payee" class="form-control col-md-7 col-xs-12" name="t_payee" required value="<?php echo isset($_GET['editReceipt'])?$receiptRow['t_payee']:'' ?>">
                      </div>
                    </div> 

              <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="receipt_made_by">Remarks
                      </label>
                      <div class="col-md-3 col-sm-3 col-xs-12">
                        <input type="text" id="t_remarks" class="form-control col-md-7 col-xs-12" name="t_remarks" value="<?php echo isset($_GET['editReceipt'])?$receiptRow['t_remarks']:'' ?>" >
                      </div>
                    </div>  



                    <div class="ln_solid"></div>
                    <div class="form-group">
                      <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <button type="submit" class="btn btn-primary" name = "<?php echo isset($_GET['editReceipt'])?'update_receipt':'save_receipt' ?>">Save Receipt</button>

                      </div>
                    </div>

                    <input type="hidden" name="t_id" value="<?php echo isset($_GET['editReceipt'])?$receiptRow['t_id']:'' ?>">

                  </form>
                </div>
              </div>
            </div>
          </div>


        </div>
        <div class="clearfix" style="height: 120px"></div>

        <!-- footer content -->

        <?php include 'footer.php';  ?>
        <!-- /footer content -->

      </div>
      <!-- /page content -->
    </div>

  </div>

  <div id="custom_notifications" class="custom-notifications dsp_none">
   <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
   </ul>
   <div class="clearfix"></div>
   <div id="notif-group" class="tabbed_notifications"></div>
 </div>

 <script src="js/bootstrap.min.js"></script>

 <!-- bootstrap progress js -->
 <script src="js/progressbar/bootstrap-progressbar.min.js"></script>
 <script src="js/nicescroll/jquery.nicescroll.min.js"></script>
 <!-- icheck -->
 <script src="js/icheck/icheck.min.js"></script>

 <script src="js/custom.js"></script>

 <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
 <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>



 <script src="js/pace/pace.min.js"></script>
 <script>




  

  $('#chequeDetails').hide();
  $('#paymentDetailsGroup').hide();
  $('#actualDonationAmount').hide();


  <?php


    echo $disabled?"$(':input').prop('disabled', true);":"";


    if(isset($_GET['editReceipt'])) {

      $jsDump =  "
                  $('#t_type_id').val(".$receiptRow['t_type_id'].");
                  $('#t_mode').val('".$receiptRow['t_mode']."');
                ";

      if($receiptRow['t_mode']=='cash' || $receiptRow['t_mode']=='cheque'){
          $jsDump = $jsDump."$('#donation_amount').val(".$receiptRow['t_credit']."); ";

         if($receiptRow['t_mode']=='cheque') {

            $jsDump = $jsDump."$('#chequeDetails').show();";
            $jsDump = $jsDump."$('#cheque_no').val(".$receiptRow['t_cheque_no']."); ";

          }  
      } else {

              $jsDump = $jsDump."$('#paymentDetailsGroup').show();";
              $jsDump = $jsDump."$('#donationAmountCashCheque').hide();";
              $paymentAmounts = explode('|', $receiptRow['t_payment_amount']);
              $paymentDates = explode('|', $receiptRow['t_payment_date']);
              $jsDump = $jsDump."$('.donationAmount').val(".$paymentAmounts[0]."); ";
              $jsDump = $jsDump."$('.paymentDate').val('".$paymentDates[0]."'); ";

              if($receiptRow['t_is_part']=='1' && sizeof($paymentAmounts)==1){

                $jsDump = $jsDump."$('.removeElementButton').hide();";
                $jsDump = $jsDump."$('#actualDonationAmount').show();";
                $jsDump = $jsDump."$('#partialClaim').val(1); ";
                $jsDump = $jsDump."$('#actual_amount').val(".$receiptRow['t_actual_amount']."); ";
                

              } else {
                $jsDump = $jsDump."$('#partialClaim').val(0); ";
                $jsDump = $jsDump."$('#partialClaimSection').hide(); ";

                if(sizeof($paymentAmounts)>1){
                  $i = 0;
                  $template = '';
                  foreach ($paymentAmounts as $paymentAmount) {
                    if($i!=0) {
                      $template = $template."<div class='form-group' id = '" .($i*108)."'>  <label class='control-label col-md-3 col-sm-3 col-xs-12' >  Payment <span class='paymentslno'>".($i+1)."</span> *  </label>   <div class='col-md-3 col-sm-3 col-xs-12'> <input type='number' class='form-control col-md-7 col-xs-12 donationAmount' name='donationAmount[]' value='".$paymentAmount."' placeholder='Donation Amount' onkeyup='getTotalAmount()'>  </div>    <div class='col-md-3 col-sm-3 col-xs-12'>                        <input id = 'date".($i*108)."' class='form-control col-md-7 col-xs-12 paymentDate' type='text' name = 'paymentDate[]' value='".$paymentDates[$i]."' autocomplete='off' placeholder='Donation Date'>     </div>  <div class='col-md-3 col-sm-3 col-xs-12 removeElementButton'>     <a class='btn btn-danger' onclick='removeElement(" .($i*108). ")'><i class='glyphicon glyphicon-trash'></i></a>  </div>  </div>";
                    }
                    
                    $i++;
                  }

                  $jsDump = $jsDump."$('#eopd').append(\" ".$template." \");";

                }

              }
              $jsDump = $jsDump."$('#totalDonationAmount').html(".$receiptRow['t_credit']."); ";
          }

      echo $jsDump;

    } else {
      echo "$('.removeElementButton').hide();";
    }
  ?>


   $(function () {
    $("#dateabc, .paymentDate").datepicker({
      changeMonth: true,
      changeYear: true,
      dateFormat:"dd-mm-yy",
      yearRange: "-10:+0",
      maxDate: new Date()

   });
  });



 /*  const input = document.querySelector('input[name="donor_address_4"]');

input.addEventListener('invalid', function (event) {
  event.target.setCustomValidity('');
  if (!event.target.validity.valid) {
    event.target.setCustomValidity('Please enter a valid PIN code');
  }
})*/

document.addEventListener("DOMContentLoaded", function() {
    var elements = document.querySelector('input[name="t_donor_address_4"]');
        elements.oninvalid = function(e) {
            e.target.setCustomValidity("");
            if (!e.target.validity.valid) {
                e.target.setCustomValidity("Please enter a valid PIN code");
            }
        };
        elements.oninput = function(e) {
            e.target.setCustomValidity("");
        };


         var elements1 = document.querySelector('input[name="t_donor_mobile"]');
        elements1.oninvalid = function(e) {
            e.target.setCustomValidity("");
            if (!e.target.validity.valid) {
                e.target.setCustomValidity("Please enter a valid 10 digit Mobile No.");
            }
        };
        elements1.oninput = function(e) {
            e.target.setCustomValidity("");
        };
})

function getPaymentsCount() {
  return $('.donationAmount').length;
}

function getTotalAmount() {
  var onlineDonationAmount = +0;
  $('.donationAmount').each(function(i) {
    onlineDonationAmount+= +$(this).val();
});
  $('#totalDonationAmount').html(onlineDonationAmount);

  return onlineDonationAmount;
}


function postElementUpdate() {

  var count = getPaymentsCount();
  if (count == 1) {
    $('#partialClaimSection').show(500);
    $('.removeElementButton').hide();
  } else {
        $('#partialClaimSection').hide(500);
        $('.removeElementButton').show();
  }

  getTotalAmount();

  $('.paymentslno').each(function(i) {
    $(this).html(i+1);
});


}


function addElement() {

  if(getPaymentsCount() == 6) {
    alert('A Maximum of 6 online payments can be claimed in a single donation receipt. Pls make a separate receipt to claim further donations.');
    return false;
  }

  var d = new Date();
  var uniqueId = d.getTime();  
  var template = "<div class='form-group' id = " + uniqueId +">  <label class='control-label col-md-3 col-sm-3 col-xs-12' >  Payment <span class='paymentslno'></span> *  </label>   <div class='col-md-3 col-sm-3 col-xs-12'> <input type='number' class='form-control col-md-7 col-xs-12 donationAmount' name='donationAmount[]' placeholder='Donation Amount' onkeyup='getTotalAmount()'>  </div>    <div class='col-md-3 col-sm-3 col-xs-12'>                        <input id = 'date"+uniqueId+"' class='form-control col-md-7 col-xs-12 paymentDate' type='text' name = 'paymentDate[]' autocomplete='off' placeholder='Donation Date'>     </div>  <div class='col-md-3 col-sm-3 col-xs-12 removeElementButton'>     <a class='btn btn-danger' onclick='removeElement(" + uniqueId + ")'><i class='glyphicon glyphicon-trash'></i></a>  </div>  </div>";
 

  $('#eopd').append(template);

  var dateSelector = '#date'+uniqueId;

    $(dateSelector).datepicker({
      changeMonth: true,
      changeYear: true,
      dateFormat:"dd-mm-yy",
      yearRange: "-10:+0",
      maxDate: new Date()

   });

  postElementUpdate();
  
}



function removeElement(uniqueId) {
  uniqueId = '#' + uniqueId;
  $(uniqueId).remove();
  postElementUpdate();

}


function modeChanged() {
  var mode = $('#t_mode').val();
  if(mode == 'cash') {
    $('#donationAmountCashCheque').show(500);
    $('#chequeDetails').hide(500);
    $('#paymentDetailsGroup').hide(500);
  } else if(mode == 'online') {
    $('#donationAmountCashCheque').hide(500);
    $('#chequeDetails').hide(500);
    $('#paymentDetailsGroup').show(500);

  } else {
    $('#donationAmountCashCheque').show(500);
    $('#chequeDetails').show(500);
    $('#paymentDetailsGroup').hide(500);
  }

}

function isPartialClaim() {
  var ispartial = $('#partialClaim').val();
  if(ispartial == '1') {
    $('#actualDonationAmount').show(500);
  } else {
    $('#actualDonationAmount').hide(500);
  }
}

function validatePAN(pan_no) {
  var regpan = /^([a-zA-Z]){5}([0-9]){4}([a-zA-Z]){1}?$/;
  return regpan.test(pan_no);
}

function getDonationAmount() {
  var t_mode = $('#t_mode').val();
  return (t_mode=='online')?getTotalAmount():$('#donation_amount').val();
}

function validateChequeNo() {
  var cheque_no = $('#cheque_no').val();
  var regcheque = /^([0-9]){6}?$/;
  return regcheque.test(cheque_no);
}

function validateReceipt() {

  var t_type_id = $('#t_type_id').val();
  var t_mode = $('#t_mode').val();
  var pan_no = $('#pan_no').val();
  var donation_amount = $('#donation_amount').val();



  if(t_type_id == '2' && t_mode == 'cash') {
    alert ('80G Receipt cannot be made for cash donations.');
    return false;
  }

  if(t_mode != 'online' && (donation_amount == '' || donation_amount < 1))  {
    alert ('Please enter donation amount');
    return false;
  }

  if(t_mode == 'cheque' && !validateChequeNo()) {
    alert ('Please enter a valid cheque no.');
    return false;
  }

  if(t_mode == 'online') {

    var returnValue = true;
    $('.donationAmount').each(function(i) {
      var amount= $(this).val();
      if (amount == '' || amount < 1) {
        alert ('Please enter Online Donation Amount(s)');
        returnValue = false;
        return false;
      }
    });

    if(!returnValue){
      return false;
    }

    returnValue = true;
    $('.paymentDate').each(function(i) {
      var payment_date = $(this).val();
      if (payment_date == '') {
        alert ('Please enter Online Donation Date(s)');
        returnValue = false;
        return false;
      }
    });

    if(!returnValue){
      return false;
    }

    var partialClaim = $('#partialClaim').val();
    var actual_amount = $('#actual_amount').val();
    if (partialClaim == 'yes' && (actual_amount = '' || actual_amount < 1)) {
      alert('Please enter actual donation amount corresponding to partial claim');
      return false;
    }

  }

  if(t_type_id == '2' && !validatePAN(pan_no)){
    alert ('PAN Card No. is mandatory for 80G receipt. Please enter a valid PAN No.');
    return false;
  }

  var totalDonationAmount = getDonationAmount();

  if(t_type_id == '1' && totalDonationAmount >= 15000 && !validatePAN(pan_no)) {
    alert ('PAN Card No. is mandatory for NON-80G donation receipt above Rs. 15,000. Please enter a valid PAN No.');
    return false;

  }


  return true;
}


</script>

</body>

</html>
