<?php 
session_start();

include 'header.php'; 

$coursesql = "select * from course";

$courseresult = mysqli_query($con, $coursesql);

$centersql = "select * from center";

$centerresult = mysqli_query($con, $centersql);

$classsql = "select * from class";

$classresult = mysqli_query($con, $classsql);


?>

<!-- page content -->

<link href="css/jquery-ui.css" rel="stylesheet">
<div class="right_col" role="main">
  <div class="">
    <div class="page-title">

    </div>
    <div class="clearfix"></div>
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Upload Bulk Donation Receipt</h2>
            
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <br />
            <form id="demo-form2" class="form-horizontal form-label-left"  action = "update.php"  method = "post" enctype='multipart/form-data' >

              <h4 align = "center"> Download the Empty CSV File with column headers below, populate donation data in relevant columns and then upload to save </h4>

              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Empty CSV File<span class="required"></span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                <a href="csv/studentData.csv" class="btn btn-success" download>Download CSV </a>
                </div>
              </div>

              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Course</label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <select class="form-control" required name="s_course_id">
                    <option value="">Choose option</option>

                    <?php

                    while($courserow = mysqli_fetch_assoc($courseresult)){

                      ?>
                      
                      <option value="<?php echo $courserow['course_id'] ;?>"><?php echo $courserow['course_title'] ;?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>


                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12">Class</label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <select class="form-control" required name="s_class_id">
                      <option value="">Choose option</option>
                      <?php

                      while($classrow = mysqli_fetch_assoc($classresult)){

                        ?>

                        <option value="<?php echo $classrow['class_id'] ;?>"><?php echo $classrow['class_description'] ;?></option>
                        <?php } ?>
                      </select>
                    </div>
                  </div>


                  <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Center</label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                      <select class="form-control" required name="s_center_id">
                        <option value="">Choose option</option>
                        <?php

                        while($centerrow = mysqli_fetch_assoc($centerresult)){

                          ?>

                          <option value="<?php echo $centerrow['center_id'] ;?>"><?php echo $centerrow['center_name'] ;?></option>
                          <?php } ?>
                        </select>
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="ts_topic">Select CSV File<span class="required"></span>
                      </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type='file' name='file_name' required />
                        <!-- <input type="text" id="ts_topic" required="required" class="form-control col-md-7 col-xs-12" name="ts_topic"> -->
                      </div>
                    </div>

                    <div class="ln_solid"></div>
                    <div class="form-group">
                      <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <button type="submit" class="btn btn-primary" name = "upload_students">Upload</button>

                      </div>
                    </div>

                  </form>
                </div>
              </div>
            </div>
          </div>





        </div>
        <div class="clearfix"></div>







        <!-- footer content -->

        <?php include 'footer.php';  ?>
        <!-- /footer content -->

      </div>
      <!-- /page content -->
    </div>

  </div>

  <div id="custom_notifications" class="custom-notifications dsp_none">
   <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
   </ul>
   <div class="clearfix"></div>
   <div id="notif-group" class="tabbed_notifications"></div>
 </div>

 <script src="js/bootstrap.min.js"></script>

 <!-- bootstrap progress js -->
 <script src="js/progressbar/bootstrap-progressbar.min.js"></script>
 <script src="js/nicescroll/jquery.nicescroll.min.js"></script>
 <!-- icheck -->
 <script src="js/icheck/icheck.min.js"></script>

 <script src="js/custom.js"></script>

 <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
 <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>



 <script src="js/pace/pace.min.js"></script>

</body>

</html>
