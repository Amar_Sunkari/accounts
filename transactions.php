<?php 

session_start();

include 'header.php';

?>

<!-- page content -->

<style type="text/css">
.modal-dialog{
  overflow-y: initial !important
}
.modal-body{
  height: 300px;
  overflow-y: auto;
}

</style>


<div class="right_col" role="main">
  <div class="">
    <div class="page-title">

    </div>
    <div class="clearfix"></div>



  </div>
  <div class="clearfix"></div>

  <div>


    <?php 

      if(!isset($_GET['from_date'])){
      $from_date = date('d-m-Y', strtotime('-3 month'));
      $to_date = date('d-m-Y');
    } else {
      $from_date = $_GET['from_date'];
      $to_date = $_GET['to_date'];
    }

     $costcentersql = "select * from costcenter";

          // echo $costcentersql;

          $result = mysqli_query($con, $costcentersql);

          $costcenters = '';
    ?>
    <form method="get">

      <div class="col-md-2 col-sm-12 col-xs-12">
        <select class="form-control" name = "costcenter" >
          <?php

          if(!isset($_GET['costcenter'])) {
            $_GET['costcenter'] = $_SESSION['loggedin']['c_id'];
          }


          while($costcenterrow = mysqli_fetch_assoc($result)){

            ?>
            <option  <?php if(isset($_GET['costcenter']) && $costcenterrow['c_id']==$_GET['costcenter']) { ?> selected  <?php }  ?> value = "<?php echo $costcenterrow['c_id'];  $costcenters=$costcenters.$costcenterrow['c_id'].",";?>"><?php echo $costcenterrow['c_name'];  ?></option>

            <?php

          }
          $costcenters =  substr($costcenters,0,strlen($costcenters)-1);

          
          ?>

          <option  <?php if(isset($_GET['costcenter']) && $_GET['costcenter']==$costcenters) { ?> selected  <?php }  ?> value = "<?php echo $costcenters ?>">All Cost Centers</option>
        </select>

        <?php  

        if(isset($_GET['costcenter']))
            $costcenters = $_GET['costcenter'];

        ?>


      </div>


      <?php  

        $query_type_array = array("0,1"=>"Cost Center Statement", "1"=>"Donation History", "0"=>"Red Slip History");

      ?>

      <div class="col-md-2 col-sm-12 col-xs-12">
        <select class="form-control" name = "query_type"  required>

          <?php
            foreach($query_type_array as $x => $x_value) {

              $x = strval($x);

              echo "<option value = '".$x."' ".((isset($_GET['query_type']) && $_GET['query_type'] == $x)?' selected ':' ')."> ".$x_value." </option>";;

            }   
          ?>
         
         </select>

      </div>


      <?php 

        $t_approve_status_array = array("0,1,2"=>"All Slips & Receipts", "1"=>"All Approved", "0"=>"Yet to be Approved", "2"=>"All Cancelled");

      ?>

      <div class="col-md-2 col-sm-12 col-xs-12">
        <select class="form-control" name = "t_approve_status"  required>
            <?php
            foreach($t_approve_status_array  as $x => $x_value) {

              $x = strval($x);

              echo "<option value = ".$x." ".((isset($_GET['t_approve_status']) && $_GET['t_approve_status'] == $x)?' selected ':' ')."> ".$x_value." </option>";;

            }   
          ?>
         </select>

      </div>


  <div class="col-md-2 col-sm-12 col-xs-12">
        <input id="date01" class="form-control col-md-7 col-xs-12" required="required" type="text" name = "from_date" placeholder="From Date" value = "<?php echo $from_date; ?>">
      </div>

      <div class="col-md-2 col-sm-12 col-xs-12">
        <input id="date02" class="form-control col-md-7 col-xs-12" required="required" type="text" name = "to_date" placeholder="To Date" value = "<?php echo $to_date; ?>">
      </div>
      <div class="col-md-2 col-sm-12 col-xs-12">
        <input type = "submit" class="btn btn-warning" value = "Get Transactions" >

      </div>

    </form>


  </div>
  <div class="clearfix"></div>



  <div class="row">

    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h2>Transactions from <?php echo $from_date.' to '.$to_date ?><small></small></h2>

          <div class="clearfix"></div>
        </div>
        <div class="x_content">

          <table id="datatable-responsive" class="table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
              <tr>

                <th>Sl No</th>
                <th>Receipt No.</th>
                <th>Approval Status</th>
                <th>Date Created</th>
                <th>Particulars</th>
                <th>Description</th>
                <th>Mode</th>
                <th>CostCenter</th>
                <th>Giver/Receiver</th>
                <th>Debit</th>
                <th>Credit</th>
                <th>Remarks</th>

              </tr>
            </thead>
            <tbody>

              <?php

              $costcentersqlsubquery = " t_from_costcenter_id = ". $_SESSION['loggedin']['c_id'];
              $historytypesqlsubquery = " t_type_id in ( 0,1 ) ";
              $approvalstatussqlsubquery = " t_approve_status in ( 1 ) ";

              if(isset($_GET['costcenter'])) {
                $costcentersqlsubquery = " t_from_costcenter_id in ( ".$_GET['costcenter']." ) ";
              }

              if(isset($_GET['query_type'])) {
                $historytypesqlsubquery = " t_type_id in ( ".$_GET['query_type']." ) ";
              }

              if(isset($_GET['t_approve_status'])) {
                $approvalstatussqlsubquery = " t_approve_status in ( ".$_GET['t_approve_status']." ) ";
              }




              $transactionsql = "select 
              c1.c_name as from_cost_center_name,
              c2.c_name as to_cost_center_name,
              transaction.* from transaction 
              inner join costcenter c1 
              on t_from_costcenter_id = c1.c_id 
              inner join costcenter c2 
              on t_to_costcenter_id = c2.c_id  
              inner join transactiontype 
              on t_type_id = transactiontype.type_id 
              where  
              " .$costcentersqlsubquery. " and 
              " .$historytypesqlsubquery. " and 
              " .$approvalstatussqlsubquery. " and 
              t_date >= '".convertdatedb($from_date)."' and 
              t_date <= '".convertdatedb($to_date)."' 
              order by t_id desc";

              // echo $transactionsql;


              $result = mysqli_query($con, $transactionsql);

              $slno=0;

              while ($transactionrow = mysqli_fetch_assoc($result)) {

                $slno++;

                ?>

                <tr>

                   <td>
                    <?php echo $slno ?>
                  </td>
                   <td>
                  2
                  </td>

                  <td>
                    <?php echo convertdatenormal($transactionrow['t_date']) ?>
                  </td>
                  <td>
                    <?php echo ($transactionrow['t_approve_status']==0)?'To Be Approved':(($transactionrow['t_approve_status']==1)?'Approved':'Cancelled') ?>
                  </td>
                   <td>
                    <?php echo $transactionrow['type_name'] ?>
                  </td>
                  <td>
                    <?php echo $transactionrow['t_detail'] ?>
                  </td>
                  <td>
                    <?php echo $transactionrow['t_mode'] ?>
                  </td>
                  <td>
                    <?php echo $transactionrow['to_cost_center_name'] ?>
                  </td>
                  <td>
                    <?php echo $transactionrow['from_cost_center_name'] ?>
                  </td>
                  <td>
                    <?php echo $transactionrow['t_debit'] ?>
                  </td>
                  <td>
                    <?php echo $transactionrow['t_credit'] ?>
                  </td>
                  <td>
                    <?php echo $transactionrow['t_remarks'] ?>
                  </td>
                  </tr>

                  <?php

                }
                ?>
              </tbody>
            </table>


          </div>
        </div>
      </div>
      <div class="col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 50px;">

        <a type="button" class="btn btn-warning" href = "createtransaction.php">
        
        Create Transaction </a>

      </div>
    </div>







    <div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-hidden="true" id="comment_modal">
      <div class="modal-dialog modal-sm">
        <div class="modal-content">

          <form action="update.php" method="post">

            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
              </button>
              <h4 class="modal-title" id="myModalLabel2">User Details</h4>
            </div>
            <div class="modal-body">


              <input type="hidden" name="u_id" id="u_id">


              <div class="form-group">
                <label class="control-label col-md-4 col-sm-4 col-xs-6">Name<span class="required"></span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input id="u_name" class="form-control col-md-12 col-xs-12" type="text" name="u_name" required>
                </div>
              </div>
              <br>
              <br>

               <div class="form-group">
                <label class="control-label col-md-4 col-sm-4 col-xs-6">Email id<span class="required"></span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input id="u_email" class="form-control col-md-12 col-xs-12" type="text" name="u_email" required>
                </div>
              </div>
              <br>
              <br>


              <div class="form-group">
                <label class="control-label col-md-4 col-sm-4 col-xs-6">Password<span class="required"></span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input id="u_password" class="form-control col-md-12 col-xs-12" type="text" name="u_password" required>
                </div>
              </div>
              <br>
              <br>

              <div class="form-group">
                <label class="control-label col-md-4 col-sm-4 col-xs-6">Mobile No<span class="required"></span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input id="u_mobile" class="form-control col-md-12 col-xs-12" type="text" name="u_mobile" required>
                </div>
              </div>
              <br>
              <br>
                


              <div class="form-group">
                <label class="control-label col-md-4 col-sm-4 col-xs-6">Role<span class="required"></span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <select class="form-control" name="u_role" id="u_role" required>

                  
                    <option value="">Select</option>
                    <option value="admin">Admin</option>
                    <option value="accountant">Accountant</option>
                    <option value="hod">HOD</option>
                    <option value="regular_user">Regular User</option>

                  </select>
                </div>
              </div>

              <br>
              <br>

              <br>
              <br>


            </div>
            <div class="modal-footer">


              <button class="btn btn-primary" type="submit" id = "user_update" name="update_user">Update</button>
            </div>

          </form>

        </div>
      </div>
    </div>





    <!-- footer content -->

    <?php include 'footer.php'; ?>
    <!-- /footer content -->

  </div>
  <!-- /page content -->
</div>

</div>

<div id="custom_notifications" class="custom-notifications dsp_none">
  <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
  </ul>
  <div class="clearfix"></div>
  <div id="notif-group" class="tabbed_notifications"></div>
</div>

<script src="js/bootstrap.min.js"></script>

<!-- bootstrap progress js -->
<script src="js/progressbar/bootstrap-progressbar.min.js"></script>
<script src="js/nicescroll/jquery.nicescroll.min.js"></script>
<!-- icheck -->
<script src="js/icheck/icheck.min.js"></script>

<script src="js/custom.js"></script>



<!-- Datatables -->
<!-- <script src="js/datatables/js/jquery.dataTables.js"></script>
  <script src="js/datatables/tools/js/dataTables.tableTools.js"></script> -->

  <!-- Datatables-->
  <!-- <script src="js/jquery.min.js"></script> -->
  <script src="js/datatables/jquery.dataTables.min.js"></script>
  <script src="js/datatables/dataTables.bootstrap.js"></script>
  <script src="js/datatables/dataTables.buttons.min.js"></script>
  <script src="js/datatables/buttons.bootstrap.min.js"></script>
  <script src="js/datatables/jszip.min.js"></script>
  <script src="js/datatables/pdfmake.min.js"></script>
  <script src="js/datatables/vfs_fonts.js"></script>
  <script src="js/datatables/buttons.html5.min.js"></script>
  <script src="js/datatables/buttons.print.min.js"></script>
  <script src="js/datatables/dataTables.fixedHeader.min.js"></script>
  <script src="js/datatables/dataTables.keyTable.min.js"></script>
  <script src="js/datatables/dataTables.responsive.min.js"></script>
  <script src="js/datatables/responsive.bootstrap.min.js"></script>
  <script src="js/datatables/dataTables.scroller.min.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script> 


  <!-- pace -->
  <script src="js/pace/pace.min.js"></script>



  <script>


 $(function () {
  $("#date01").datepicker({
    changeMonth: true,
    changeYear: true,
    dateFormat:"dd-mm-yy",
    yearRange: "-100:+0",
   maxDate: new Date()

 });

  $("#date02").datepicker({
    changeMonth: true,
    changeYear: true,
    dateFormat:"dd-mm-yy",
    yearRange: "-100:+0",
   maxDate: new Date()

 });
});
</script>


  <script>

  //   $(function () {
  //     $("#t_dob").datepicker({
  //     changeMonth: true, 
  //     changeYear: true, 
  //     dateFormat:"dd-mm-yy",
  //     yearRange: "-90:+00",
  //     maxDate: new Date()

  //   });
  // });



    var handleDataTableButtons = function () {
      "use strict";
      0 !== $("#datatable-responsive").length && $("#datatable-responsive").DataTable({
        scrollX: true,
        keys: true,
        fixedHeader: true,
        dom: "Blfrtip",
        rowCallback: function(row, data, index){
          if(data[3] == 'To Be Approved'){
            $(row).css("background-color", "rgb(255 165 0 / 15%)");
          } else if(data[3] == 'Approved'){
            $(row).css("background-color", "rgb(43 241 47 / 15%)");
          } else if(data[3] == 'Cancelled'){
            $(row).css("background-color", "rgb(241 66 43 / 15%)");
          }
          
        },
        lengthMenu: [
        [5, 10, 25, 50, -1],
        [5, 10, 25, 50, "All"]
        ],
        buttons: [{
          extend: "copy",
          className: "btn-sm",
          exportOptions: {
            columns: [1, 2, 3, 4, 5, 6]
          }
        }, {
          extend: "csv",
          className: "btn-sm",
          exportOptions: {
            columns: [1, 2, 3, 4, 5, 6]
          }
        }, {
          extend: "excel",
          className: "btn-sm",
          exportOptions: {
            columns: [1, 2, 3, 4, 5, 6]
          }
        }, {
          extend: "pdf",
          className: "btn-sm",
          exportOptions: {
            columns: [1, 2, 3, 4, 5, 6]
          }
        }, {
          extend: "print",
          className: "btn-sm",
          exportOptions: {
            columns: [1, 2, 3, 4, 5, 6]
          }
        }],
      })
    },
    TableManageButtons = function () {
      "use strict";
      return {
        init: function () {
          handleDataTableButtons()
        }
      }
    }();
  </script>
  <script type="text/javascript">
    $(document).ready(function () {
      $('#datatable').dataTable();
      $('#datatable-keytable').DataTable({
        keys: true
      });
    /* $('#datatable-responsive').DataTable({
       keys: true,
       fixedHeader: true
     });*/
     $('#datatable-scroller').DataTable({
      ajax: "js/datatables/json/scroller-demo.json",
      deferRender: true,
      scrollY: 380,
      scrollCollapse: true,
      scroller: true
    });
     var table = $('#datatable-fixed-header').DataTable({
      fixedHeader: true
    });
   });
    TableManageButtons.init();
  </script>


</body>

</html>